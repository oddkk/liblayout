#ifndef LAYOUT_EVENT_H
#define LAYOUT_EVENT_H

#include <string>
#include "layout/callback.h"

namespace layout {
	class IEventReceiver;
	class Event {
	public:
		Event(std::string name="",
		      IEventReceiver* target=nullptr,
		      IEventReceiver* sender=nullptr);
		const std::string name;
		IEventReceiver* const target;
		IEventReceiver* const sender;
		bool stopPropagation;
	};

	typedef ICallbackProvider<Event&>::Callback EventCallback;
	typedef std::function<void(Event&)> EventCallbackFunction;

	class IEventReceiver : public ICallbackProvider<Event&> {
	public:
		void raise(std::string eventName);
		void raise(Event);

		void on(EventCallback);
		void on(std::string event, EventCallbackFunction);

	protected:
		virtual IEventReceiver* _getParentEventReceiver() const=0;
	};
}

#endif
