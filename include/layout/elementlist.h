#ifndef LAYOUT_ELEMENTLIST_H
#define LAYOUT_ELEMENTLIST_H

#include <vector>
#include <initializer_list>
#include "layout/declaration.h"
#include "layout/style.h"
#include "layout/event.h"

namespace layout {
	class Element;
	typedef std::vector<Element*>::iterator ElementIterator;
	typedef std::vector<Element*>::const_iterator ConstElementIterator;
	class ElementList {
	public:
		ElementList();
		ElementList(const std::vector<Element*>&);
		ElementList(std::initializer_list<Element*>);

		void append(Element*);
		void append(const ElementList&);

		void remove(Element const*);
		void removeAt(unsigned int);
		void remove(const ElementList&);
		void clear();

		unsigned int elementIndex(Element const*) const;

		bool empty() const;
		unsigned int size() const;
		Element* operator[](unsigned int) const;
		ElementIterator begin();
		ConstElementIterator begin() const;
		ElementIterator end();
		ConstElementIterator end() const;

		bool has(Element const*) const;
		bool has(const ElementList&) const;

		void forEach(void(*)(Element*, unsigned int));

		void applyStyle(const StyleRule&);
		void applyDeclaration(const Declaration&);

		void on(std::string eventName, EventCallbackFunction);
		void on(EventCallback);

	private:
		std::vector<Element*> _elements;
	};
}

#endif
