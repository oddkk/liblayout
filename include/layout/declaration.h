#ifndef LAYOUT_DECLARATION_H
#define LAYOUT_DECLARATION_H

#include "layout/selector.h"
#include "layout/expression/expression.h"
#include "layout/context.h"

#include <string>
#include <list>

namespace layout {
	class Declaration {
	public:
		Declaration();
		Declaration(std::string, Selector);
		Declaration(std::string property, Selector, expression::Expression);

		Selector selector() const;
		std::string property() const;
		expression::Value get(const Context& ctx=
		                            Context()) const;

	private:
		std::string _property;
		Selector _selector;
		expression::Expression _expr;
	};

	class IDeclarationContainer;
	class DeclarationList {
	public:
		DeclarationList(IDeclarationContainer* owner, std::string name);

		void apply(Declaration);
		void apply(const DeclarationList&);
		Declaration get() const;

		void clear();

	private:
		std::list<Declaration> _declarations;
		IDeclarationContainer* _owner;
		std::string _name;
	};

	class IDeclarationContainer {
	public:
		void applyDeclaration(const Declaration&);
		Declaration getDeclaration(std::string property);
		void clearDeclarations();

	protected:
		DeclarationList* _getDeclList(std::string property);
		virtual bool _matchesSelector(Selector selector) const=0;

	private:
		std::map<std::string,
		         std::unique_ptr<DeclarationList>> _declarations;

		friend DeclarationList;
	};
}

#endif
