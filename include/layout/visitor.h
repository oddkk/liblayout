#ifndef VISITOR_H
#define VISITOR_H

#include <map>
#include <vector>
#include <string>
#include "layout/callback.h"

namespace layout {
	class IVisitable;
	class Visitor {
	public:
		virtual void push(IVisitable*) {};
		virtual void pop(IVisitable*) {};

		virtual void visit(IVisitable*) {};
	};

	class IVisitable {
	public:
		virtual void accept(Visitor*)=0;
		virtual std::string _visitableName() const=0;
	};
}

#endif
