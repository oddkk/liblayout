#ifndef LAYOUT_FONT_H
#define LAYOUT_FONT_H

#include "layout/property.h"

namespace layout {
	class Element;
	class Font {
	public:
		void bind(Element* target);
		Property family;
		Property size;
		Property weight;
		Property underline;
		Property italic;
	};
}
#endif
