#ifndef LAYOUT_STYLE_H
#define LAYOUT_STYLE_H

#include "layout/selector.h"
#include "layout/declaration.h"
#include <string>
#include <vector>

namespace layout {
	class StyleRule {
	public:
		StyleRule();
		StyleRule(std::string rule);
		Selector selector;
		std::vector<Declaration> declarations;
	};

	class StyleSheet {
	public:
		StyleSheet();
		StyleSheet(std::string);
		std::vector<StyleRule> rules;
	};
}

#endif
