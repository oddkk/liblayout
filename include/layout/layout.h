#ifndef LAYOUT_LAYOUT_H
#define LAYOUT_LAYOUT_H

#include <string>
#include <memory>
#include <list>
#include <exception>
#include "layout/element.h"
#include "layout/selector.h"
#include "layout/vec2.h"
#include "layout/style.h"
#include "layout/context.h"

namespace layout {
	class Layout {
	public:
		Layout();
		void load(std::string data);
		void accept(Visitor*);

		Element* root() const;
		ElementList select(const Selector&) const;
		ElementList select(std::string) const;

		void applyStyle(const StyleSheet&);
		void applyStyle(const StyleRule&);

		Vec2 viewportSize() const;
		void setViewportSize(Vec2);

		float dpi() const;
		void setDpi(float);

		Context funcContext;
	private:
		std::list<StyleRule> _rules;
		std::unique_ptr<Element> _root;
		Vec2 _viewportSize;
		float _dpi;

		friend Element;
	};

	class RootNodeNotFoundException : public std::exception {
		virtual const char* what() const throw();
	};
}

#endif
