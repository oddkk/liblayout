#ifndef LAYOUT_ELEMENT_H
#define LAYOUT_ELEMENT_H

#include <string>
#include <vector>
#include <memory>
#include "layout/classlist.h"
#include "layout/visitor.h"
#include "layout/selector.h"
#include "layout/elementlist.h"
#include "layout/property.h"
#include "layout/rectangle.h"
#include "layout/style.h"
#include "layout/event.h"

#include <map>

namespace layout {
	class Layout;
	class Element
		: public IDeclarationContainer,
	      public IEventReceiver,
	      public IVisitable{
	public:
		virtual ~Element() {}
		virtual void accept(Visitor*);
		virtual std::string tagName() const = 0;
		void deleteChild(Element*);
		void deleteChild(const ElementList&);
		void appendChild(Element*);
		void appendChild(const ElementList&);
		void move(Element* newParent);

		Element* offset(int) const;

		ElementList children() const;
		ElementList decendents() const;
		ElementList siblings() const;
		ElementList select(const Selector&) const;
		ElementList select(std::string) const;

		Layout* layout() const;
		Element* parent() const;

		bool matches(const SelectorItem&) const;
		bool matches(const Selector&) const;

		std::string name;
		ClassList styleClass;
		ClassList pseudoClass;

		void applyStyle(const StyleRule&);
		void checkGlobalDeclarations();

		Context getContext();
		expression::Value operator[](std::string property);

		Rectangle rectangle;

		virtual bool requestInnerText() { return false; }
		virtual void innerText(std::string text) {}

	protected:
		Element();
		void _acceptChildren(Visitor*);

		virtual bool _matchesSelector(Selector sel) const;
		virtual IEventReceiver* _getParentEventReceiver() const;
		virtual std::string _visitableName() const;

		std::vector<std::unique_ptr<Element>> _children;
		Layout* _layout;
		Element* _parent;

		friend Property;

	public:
		static Element* construct(std::string tagName);
		static void registerElement(Element*(*construct)());
	};
}

#endif
