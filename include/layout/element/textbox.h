#ifndef ELEMENT_TEXTBOX_H
#define ELEMENT_TEXTBOX_H

#include "layout/element.h"

namespace layout {
	namespace element {
		class Textbox : public Element
		{
		public:
			Textbox();
			virtual std::string tagName() const { return "textbox"; }
		};
	}
}

#endif

