#ifndef ELEMENT_OPTION_H
#define ELEMENT_OPTION_H

#include "layout/element.h"

namespace layout {
	namespace element {
		class Option : public Element
		{
		public:
			Option();
			virtual std::string tagName() const { return "option"; }
			Property value;
			Property text;
		};
	}
}

#endif

