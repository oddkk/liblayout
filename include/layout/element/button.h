#ifndef ELEMENT_BUTTON_H
#define ELEMENT_BUTTON_H

#include "layout/element.h"
#include "layout/font.h"

namespace layout {
	namespace element {
		class Button : public Element
		{
		public:
			Button();
			virtual std::string tagName() const { return "button"; }

			Property text;
			Property text_color;
			Font font;
		};
	}
}

#endif

