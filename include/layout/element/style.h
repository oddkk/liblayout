#ifndef ELEMENT_STYLE_H
#define ELEMENT_STYLE_H

#include "layout/element.h"
#include "layout/style.h"

namespace layout {
	namespace element {
		class Style : public Element {
		public:
			Style();
			virtual std::string tagName() const { return "style"; }

			Property src;
			StyleSheet sheet;

			virtual bool requestInnerText() { return true; }
			virtual void innerText(std::string text);
		};
	}
}

#endif
