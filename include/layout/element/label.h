#ifndef ELEMENT_LABEL_H
#define ELEMENT_LABEL_H

#include "layout/element.h"

namespace layout {
	namespace element {
		class Label : public Element
		{
		public:
			Label();
			virtual std::string tagName() const { return "label"; }
		};
	}
}

#endif

