#ifndef ELEMENT_CONTAINER_H
#define ELEMENT_CONTAINER_H

#include "layout/element.h"

namespace layout {
	namespace element {
		class Container : public Element
		{
		public:
			Container();
			virtual std::string tagName() const { return "container"; }
		};
	}
}

#endif

