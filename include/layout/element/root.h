#ifndef ELEMENT_ROOT_H
#define ELEMENT_ROOT_H

#include "layout/element.h"

namespace layout {
	class Layout;
	namespace element {
		class Root : public Element
		{
		public:
			Root(Layout*);
			virtual std::string tagName() const { return "root"; }
		};
	}
}

#endif

