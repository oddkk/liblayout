#ifndef ELEMENT_SELECT_H
#define ELEMENT_SELECT_H

#include "layout/element.h"
#include "layout/font.h"

namespace layout {
	namespace element {
		class Select : public Element
		{
		public:
			Select();
			virtual std::string tagName() const { return "select"; }
			ElementList options() const;

			Property arrow_left;
			Property arrow_right;
			Property text_color;
			Font font;
		};
	}
}

#endif

