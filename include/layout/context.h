#ifndef LAYOUT_EXPRESSION_CONTEXT_H
#define LAYOUT_EXPRESSION_CONTEXT_H

#include <exception>
#include <string>
#include <memory>
#include <map>

template<typename T>
struct ContextEntryType {
	std::string _name();
	static std::string name() { return ContextEntryType<T>()._name(); }
};

#define REGISTER_CONTEXT_ENTRY_TYPE(X) template<>\
	std::string ContextEntryType<X>::_name() { return #X; }

namespace layout {
	class ContextWrongTypeException : public std::exception {
	public:
		ContextWrongTypeException(std::string name,
								  std::string dataType,
								  std::string expType);

		virtual const char* what() const throw();

		const std::string name;
		const std::string dataType;
		const std::string expectedType;

	private:
		std::string _what;
	};

	class ContextVariableNotSetException : public std::exception {
	public:
		ContextVariableNotSetException(std::string var);

		virtual const char* what() const throw();

		const std::string name;

	private:
		std::string _what;
	};

	class _ContextEntry {
	public:
		_ContextEntry(std::string name);
		const std::string name;
		virtual std::string type() const=0;
		virtual _ContextEntry* clone() const=0;
	};
	template<typename T>
	class ContextEntry : public _ContextEntry {
	public:
		ContextEntry(std::string name, T data)
			: _ContextEntry(name),
			  data(data) {}
		virtual std::string type() const {
			return ContextEntryType<T>::name();
		}
		virtual _ContextEntry* clone() const {
			return (_ContextEntry*)new ContextEntry<T>(*this);
		}
		const T data;
	};

	class Context {
	public:
		Context();
		Context(const Context&);
		template<typename T>
		void set(std::string name, T data) {
			_set((_ContextEntry*)new ContextEntry<T>(name, data));
		}

		template<typename T>
		const T& get(std::string name) const {
			_ContextEntry* entry = _get(name);
			if (!entry || entry->type() != ContextEntryType<T>::name())
				throw ContextWrongTypeException(
					name,
					entry->type(),
					ContextEntryType<T>::name());
			return ((ContextEntry<T>*)entry)->data;
		}

	private:
		void _set(_ContextEntry*);
		_ContextEntry* _get(std::string name) const;
		std::map<std::string, std::unique_ptr<_ContextEntry>> _entries;
	};
}

#endif
