#ifndef LAYOUT_RECTANGLE_H
#define LAYOUT_RECTANGLE_H

#include "layout/property.h"
#include "layout/box.h"
#include "layout/mat33.h"

namespace layout {
	class Element;
	class Rectangle {
	public:
		void bind(Element*);

		Property left;
		Property right;
		Property top;
		Property bottom;

		Property x;
		Property y;

		Property width;
		Property height;

		Property rotation;

		Box px() const;
		Box unit() const;

		Box aunit() const;

		Mat33 transformation() const;
		Mat33 fromScreenTransformation() const;

		Element* owner() const;
		Box parentBoxPx() const;
		Box parentBoxUnit() const;

	private:
		Element* _owner;
	};
}

#endif
