#ifndef LAYOUT_VEC2_H
#define LAYOUT_VEC2_H

namespace layout {
	struct Vec2 {
		float x, y;

		Vec2() : x(), y() {}
		Vec2(float x, float y) : x(x), y(y) {}

		Vec2& operator+=(const Vec2& rhs);
		Vec2& operator-=(const Vec2& rhs);
		Vec2& operator*=(const Vec2& rhs);
		Vec2& operator/=(const Vec2& rhs);

		Vec2 operator+(const Vec2& rhs) const;
		Vec2 operator-(const Vec2& rhs) const;
		Vec2 operator*(const Vec2& rhs) const;
		Vec2 operator/(const Vec2& rhs) const;
	};
}

#endif
