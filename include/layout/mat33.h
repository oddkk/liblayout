#ifndef LAYOUT_MATRIX_H
#define LAYOUT_MATRIX_H

#include "layout/vec2.h"
#include <initializer_list>

namespace layout {
	struct Mat33 {
		float data[3*3];

		Mat33();
		Mat33(float* data);
		Mat33(std::initializer_list<float>);
		const float& get(int x, int y) const;
		float& get(int x, int y);
		Mat33 operator*(const Mat33& rhs) const;
		Mat33& operator*=(const Mat33& rhs);

		Mat33 operator*(const float& rhs) const;
		Mat33& operator*=(const float& rhs);

		Mat33& operator=(const Mat33& rhs);

		static Mat33 identity();
		static Mat33 scale(float w, float h);
		static Mat33 scale(const Vec2&);
		static Mat33 translate(float x, float y);
		static Mat33 translate(const Vec2&);
		static Mat33 rotate(float theta);
	};

	Vec2 operator*(const Mat33& lhs, const Vec2& rhs);
}

#endif

