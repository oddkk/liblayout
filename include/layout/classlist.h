#ifndef CLASSLIST_H
#define CLASSLIST_H

#include <string>
#include <set>

namespace layout {
	class ClassList {
	public:
		void activate(std::string);
		void deactivate(std::string);
		bool isActive(std::string) const;
		bool hasActive(const ClassList&) const;
		bool hasAllActive(const ClassList&) const;
		bool empty() const;

		std::string to_string(std::string separator=" ") const;
	private:
		std::set<std::string> _active;
	};
}

#endif
