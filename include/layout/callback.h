#ifndef LAYOUT_CALLBACK_H
#define LAYOUT_CALLBACK_H

#include <string>
#include <map>
#include <list>
#include <functional>

namespace layout {

	template<typename... Args>
	class ICallbackProvider {
	public:
		class Callback {
		public:
			Callback(std::string event, std::function<void(Args...)> function)
				: event(event), function(function) {}

			std::string event;
			std::function<void(Args...)> function;
		};

		void bindCallback(const Callback& callback) {
			_callbacks[callback.event].push_back(callback);
		}
		void bindCallback(std::string event, std::function<void(Args...)> function) {
			bindCallback(Callback(event, function));
		}

		void raiseCallback(std::string event, Args... args) {
			for (const Callback& callback : _callbacks[event]) {
				callback.function(args...);
			}
		}

	private:
		std::map<std::string, std::list<Callback>> _callbacks;
	};
}

#endif
