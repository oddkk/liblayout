#ifndef LAYOUT_EXPRESSION_FUNCTION_H
#define LAYOUT_EXPRESSION_FUNCTION_H

#include "layout/expression/expression.h"
#include <string>
#include <exception>

namespace layout {
	namespace expression {
		class FunctionArgs {
		public:
			FunctionArgs(const std::vector<Value>& args,
			             const Context& context);
			const std::vector<Value> args;
			const Context context;
		};
		typedef Value(*Func)(const FunctionArgs&);

		class FunctionDefinition {
		public:
			FunctionDefinition();
			FunctionDefinition(int numParams, Func func);
			void eval(EvalData&);
			const int numParams;
			std::string name;
		private:
			Func _func;
		};

		FunctionDefinition getFunctionDefinition(std::string name);
		void initializeFunctions();
		void registerFunction(std::string name, FunctionDefinition def);

		class FunctionDoesNotExistException : public std::exception {
		public:
			FunctionDoesNotExistException(std::string name);
			virtual const char* what() const throw();
			std::string name;

		private:
			std::string _what;
		};

		class TooFewArgumentsException : public std::exception {
		public:
			TooFewArgumentsException(std::string func,
			                         unsigned int numGiven,
			                         unsigned int numExcpected);
			virtual const char* what() const throw();
			const std::string function;
			const unsigned int numGiven;
			const unsigned int numExpected;

		private:
			std::string _what;
		};
	}
}

#endif
