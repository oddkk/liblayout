#ifndef LAYOUT_EXPRESSION_H
#define LAYOUT_EXPRESSION_H

#include <string>
#include <stack>
#include <vector>
#include <memory>
#include <exception>
#include "layout/context.h"

namespace layout {
	namespace expression {
		enum class ValueType {
			none, text, number, boolean
		};

		class Value {
		public:
			Value();
			Value(std::string);
			Value(float);
			Value(bool);

			ValueType type() const;
			std::string text() const;
			void text(std::string);
			float number() const;
			void number(float);
			bool boolean() const;
			void boolean(bool);
			bool none() const;
			void setNone();

			Value operator+(const Value&) const;
			Value operator-(const Value&) const;
			Value operator*(const Value&) const;
			Value operator/(const Value&) const;

			std::string to_string() const;
		private:
			ValueType _type;
			std::string _text;
			float _number;
			bool _boolean;
		};

		class EvalData {
		public:
			EvalData(const Context&);
			std::stack<Value> stack;
			const Context context;
		};

		class Statement {
		public:
			virtual Statement* clone() const=0;
			virtual void eval(EvalData&) const=0;
			virtual std::string to_string() const=0;
		};

		class Litteral : public Statement {
		public:
			Litteral(Value value=Value());
			Value value;
			virtual void eval(EvalData&) const;
			virtual std::string to_string() const;
			virtual Statement* clone() const { return new Litteral(*this); }
		};

		class Function : public Statement {
		public:
			Function(std::string name="");
			std::string name;
			unsigned int numParams() const;
			virtual void eval(EvalData&) const;
			virtual std::string to_string() const;
			virtual Statement* clone() const { return new Function(*this); }
		};

		class Expression {
		public:
			Expression(const std::string& expr);
			Expression(const Expression&);
			Expression& operator=(const Expression&);
			Value eval(const Context& ctx=Context()) const;
			std::string to_string() const;
		private:
			std::vector<std::unique_ptr<expression::Statement>> _stmts;
			std::string text;
		};

		class InvalidOperatorException : public std::exception { public:
			InvalidOperatorException(const Value& lhs,
			                         const Value& rhs,
			                         std::string op);
			Value lhs;
			Value rhs;
			std::string op;
			virtual const char* what() const throw();

		private:
			std::string _what;
		};

		class ParenthesisMismatchException : public std::exception {
		public:
			ParenthesisMismatchException();
			virtual const char* what() const throw();

		private:
			std::string _what;
		};

		class PostfixFunctionTooManyParamsException : public std::exception {
		public:
			PostfixFunctionTooManyParamsException(std::string func);
			virtual const char* what() const throw();
			const std::string functionName;

		private:
			std::string _what;
		};
	}
}

#endif
