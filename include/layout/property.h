#ifndef LAYOUT_PROPERTY_H
#define LAYOUT_PROPERTY_H

#include "layout/selector.h"
#include "layout/expression/expression.h"
#include "layout/declaration.h"

namespace layout {
	class Element;
	class Property {
	public:
		Property();

		void bind(Element*, std::string name);
		std::string name() const;

		expression::Value get() const;

		void resetSet();

		void set(const expression::Value&);

		Property& operator=(const expression::Value&);
		Property& operator=(std::string);
		Property& operator=(float);
		Property& operator=(bool);
		Property& operator=(int);

		operator expression::Value() const;
		operator std::string() const;
		operator float() const;
		operator bool() const;
		operator int() const;

		bool operator==(const std::string& rhs) const;
		bool operator==(float rhs) const;
		bool operator==(bool rhs) const;
		bool operator==(int rhs) const;

		bool operator!=(const std::string& rhs) const;
		bool operator!=(float rhs) const;
		bool operator!=(bool rhs) const;
		bool operator!=(int rhs) const;

	private:
		Context _getContext() const;


		Element* _owner;
		std::string _name;
		DeclarationList* _values;
		expression::Value _setValue;
		bool _setValueSet;
	};
}

#endif
