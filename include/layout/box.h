#ifndef LAYOUT_RECT_H
#define LAYOUT_RECT_H

#include "layout/vec2.h"

namespace layout {
	struct Box {
		Box();
		Box(Vec2 topLeft, Vec2 size);
		Box(float left, float top, float width, float height);

		float top() const;
		float bottom() const;
		float left() const;
		float right() const;

		float width() const;
		float height() const;

		Vec2 topLeft() const;
		Vec2 center() const;
		Vec2 size() const;

		void top(float);
		void bottom(float, float pheight);
		void left(float);
		void right(float, float pwidth);

		void width(float);
		void height(float);

		void center(Vec2);
		void size(Vec2);

	private:
		Vec2 _topLeft;
		Vec2 _size;

	public:
		static Box fromSides(float left, float top, float right, float bottom);
		static Box fromCenter(float x, float y, float width, float height);
		static Box fromCenter(Vec2 pos, Vec2 size);
	};
}

#endif
