#ifndef SELECTOR_H
#define SELECTOR_H

#include <string>
#include <vector>
#include "layout/classlist.h"

namespace layout {
	enum class SelectorOperator {
		none, decendent, child, sibling, parent
	};

	class SelectorItem {
	public:
		SelectorItem();
		SelectorItem(std::string);

		std::string tag() const;
		std::string name() const;
		const ClassList& styleClass() const;
		const ClassList& pseudoClass() const;
		int offset() const;
		bool self() const;
		SelectorOperator op() const;
		std::string to_string() const;
		int score() const;

		bool useTag() const;
		bool useName() const;
		bool useClass() const;
		bool usePseudo() const;

	private:
		std::string _tag;
		std::string _name;
		ClassList _styleClass;
		ClassList _pseudoClass;
		int _offset;
		bool _self;
		SelectorOperator _operator;
	};

	bool operator==(const SelectorItem& lhs, const SelectorItem& rhs);
	bool operator!=(const SelectorItem& lhs, const SelectorItem& rhs);

	class Selector {
	public:
		Selector();
		Selector(std::string);

		std::string to_string() const;
		int score() const;

		Selector subsection(int begin, int end) const;

		std::vector<SelectorItem> items;
	};

	bool operator==(const Selector& lhs, const Selector& rhs);
	bool operator!=(const Selector& lhs, const Selector& rhs);
}

#endif
