#include "layout/mat33.h"
#include <algorithm>
#include <cmath>

namespace layout {
	Mat33::Mat33() {
		for (unsigned int i = 0; i < 3*3; i++) {
			data[i] = 0.0f;
		}
	}

	Mat33::Mat33(float* dt) {
		for (unsigned int i = 0; i < 3*3; i++) {
			data[i] = dt[i];
		}
	}

	Mat33::Mat33(std::initializer_list<float> l) {
		for (unsigned int i = 0;
		     i < std::min<unsigned int>(l.size(), 3*3);
		     i++) {
			data[i] = *(l.begin() + i);
		}
	}

	const float& Mat33::get(int x, int y) const {
		return data[x+y*3];
	}

	float& Mat33::get(int x, int y) {
		return data[x+y*3];
	}

	Mat33& Mat33::operator*=(const Mat33& rhs) {
		return *this = (*this) * rhs;
	}

	Mat33 Mat33::operator*(const Mat33& rhs) const {
		Mat33 r;
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				float v = 0.0f;
				for (int k = 0; k < 3; k++) {
					v += this->get(k, j) * rhs.get(i, k);
				}
				r.get(i, j) = v;
			}
		}
		return r;
	}

	Mat33& Mat33::operator*=(const float& rhs) {
		for (int i = 0; i < 3*3; i++) {
			data[i] *= rhs;
		}
		return *this;
	}

	Mat33 Mat33::operator*(const float& rhs) const {
		Mat33 r = *this;
		r *= rhs;
		return r;
	}

	Mat33& Mat33::operator=(const Mat33& rhs) {
		for (int i = 0; i < 3*3; i++) {
			data[i] = rhs.data[i];
		}
		return *this;
	}

	Mat33 Mat33::identity() {
		return {
			1.0f, 0.0f, 0.0f,
			0.0f, 1.0f, 0.0f,
			0.0f, 0.0f, 1.0f
		};
	}

	Mat33 Mat33::scale(const Vec2& s) {
		return scale(s.x, s.y);
	}

	Mat33 Mat33::scale(float w, float h) {
		return {
			w,    0.0f, 0.0f,
			0.0f, h,    0.0f,
			0.0f, 0.0f, 1.0f
		};
	}

	Mat33 Mat33::translate(float x, float y) {
		return {
			1.0f, 0.0f, x,
			0.0f, 1.0f, y,
			0.0f, 0.0f, 1.0f
		};
	}

	Mat33 Mat33::translate(const Vec2& s) {
		return translate(s.x, s.y);
	}

	Mat33 Mat33::rotate(float theta) {
		return {
			std::cos(theta),  std::sin(theta), 0.0f,
			-std::sin(theta), std::cos(theta), 0.0f,
			0.0f,             0.0f,            1.0f
		};
	}

	Vec2 operator*(const Mat33& lhs, const Vec2& rhs) {
		Mat33 mr = {
			rhs.x, 0, 0,
			rhs.y, 0, 0,
			0,     0, 0
		};
		Mat33 res = lhs * mr;
		return Vec2(res.get(0, 0), res.get(0, 1));
	}
}
