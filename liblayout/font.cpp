#include "layout/font.h"

namespace layout {
	void Font::bind(Element* target) {
		family.bind(target, "font-family");
		size.bind(target, "font-size");
		weight.bind(target, "font-weigth");
		underline.bind(target, "font-underline");
		italic.bind(target, "font-italic");
	}
}

