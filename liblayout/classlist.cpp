#include "layout/classlist.h"

namespace layout {
	void ClassList::activate(std::string cls) {
		_active.insert(cls);
	}

	void ClassList::deactivate(std::string cls) {
		_active.erase(cls);
	}

	bool ClassList::isActive(std::string cls) const {
		return _active.find(cls) != _active.end();
	}

	bool ClassList::hasActive(const ClassList& cl) const {
		for (auto iter : cl._active) {
			if (isActive(iter))
				return true;
		}
		return false;
	}

	bool ClassList::hasAllActive(const ClassList& cl) const {
		for (auto iter : cl._active) {
			if (!isActive(iter))
				return false;
		}
		return true;
	}

	bool ClassList::empty() const {
		return _active.empty();
	}

	std::string ClassList::to_string(std::string separator) const {
		std::string res;
		for (auto iter : _active)
			res += separator + iter;
		return res;
	}
}
