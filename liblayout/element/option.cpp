#include "layout/element/option.h"

namespace layout {
	namespace element {
		Option::Option() {
			value.bind(this, "value");
			text.bind(this, "text");
		}
	}
}

