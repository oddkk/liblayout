#include "layout/element/select.h"
#include "utf8.h"

namespace layout {
	namespace element {
		Select::Select() {
			arrow_left.bind(this, "arrow-left");
			arrow_left.bind(this, "arrow-right");
			text_color.bind(this, "text-color");
			font.bind(this);
		}

		ElementList Select::options() const {
			return select("> option");
		}
	}
}
