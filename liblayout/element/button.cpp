#include "layout/element/button.h"

namespace layout {
	namespace element {
		Button::Button() {
			text.bind(this, "text");
			text_color.bind(this, "text-color");
			font.bind(this);
		}
	}
}

