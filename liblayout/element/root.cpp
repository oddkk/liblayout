#include "layout/element/root.h"

namespace layout {
	namespace element {
		Root::Root(Layout* layout) {
			name = "root";
			_layout = layout;
		}
	}
}
