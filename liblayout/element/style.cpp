#include "layout/element/style.h"
#include "layout/layout.h"

namespace layout {
	namespace element {
		void Style_onAdded(Event& e) {
			if (e.sender == e.target) {
				Style* element = (Style*)e.target;
				element->layout()->applyStyle(element->sheet);
			}
		}

		Style::Style() {
			src.bind(this, "src");
			on("added", Style_onAdded);
		}

		void Style::innerText(std::string text) {
			sheet = StyleSheet(text);
		}

	}
}

