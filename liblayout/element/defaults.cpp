#include "element/defaults.h"
#include "layout/element.h"

#include "layout/element/button.h"
#include "layout/element/container.h"
#include "layout/element/label.h"
#include "layout/element/select.h"
#include "layout/element/option.h"
#include "layout/element/textbox.h"
#include "layout/element/style.h"

static bool isInitialized = false;

namespace layout {
	void initializeDefaultElements() {
		isInitialized = true;
		Element::registerElement(
				[]{ return (Element*)new element::Button(); });
		Element::registerElement(
				[]{ return (Element*)new element::Container(); });
		Element::registerElement(
				[]{ return (Element*)new element::Label(); });
		Element::registerElement(
				[]{ return (Element*)new element::Select(); });
		Element::registerElement(
				[]{ return (Element*)new element::Textbox(); });
		Element::registerElement(
				[]{ return (Element*)new element::Style(); });
		Element::registerElement(
				[]{ return (Element*)new element::Option(); });
	}
}

