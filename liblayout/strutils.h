#ifndef STRUTILS_H
#define STRUTILS_H

#include <string>
#include <vector>

namespace layout {
	namespace str {
		typedef std::string::iterator iterator;
		std::vector<std::string> split(std::string, unsigned int);
		std::string readWhile(iterator& it,
		                      const iterator& end,
		                      bool(*func)(unsigned int));
		std::string readUntil(iterator& it,
		                      const iterator& end,
		                      unsigned int c);
		void trimWhitespace(iterator& it, const iterator& end);
		std::string trimWhitespace(std::string str);

		bool isWhitespace(unsigned int c);
		bool isNumber(unsigned int c);
		bool isInteger(unsigned int c);
		bool isAlphabetic(unsigned int c);
		bool isWord(unsigned int c);
		bool isSymbol(unsigned int c);
		bool isText(unsigned int c);
	}
}

#endif

