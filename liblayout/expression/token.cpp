#include "expression/token.h"

namespace layout
{
	namespace expression
	{
		static const struct _sym_info {
			Symbol symbol;
			std::string op;
			int precedence;
		} symbols[] = {
			{ Symbol::none,				" ", 0  },
			{ Symbol::pluss,			"+", 4  },
			{ Symbol::minus,			"-", 4  },
			{ Symbol::multiply,			"*", 3  },
			{ Symbol::divide,			"/", 3  },
			{ Symbol::modulo,			"%", 3  },
			{ Symbol::parenthesis_open, "(", 1  },
			{ Symbol::parenthesis_close,")", 1  },
			{ Symbol::comma,			",", 14 },
		};
		static const size_t symbols_size = sizeof(symbols) /
		                                   sizeof(symbols[0]);

		static _sym_info getSymInfo(Symbol sym) {
			for (size_t i = 0; i < symbols_size; i++) {
				if (symbols[i].symbol == sym)
					return symbols[i];
			}
			return symbols[0];
		}

		Symbol stringToSymbol(std::string str) {
			for (size_t i = 0; i < symbols_size; i++) {
				if (symbols[i].op == str)
					return symbols[i].symbol;
			}
			return Symbol::none;
		}

		std::string symbolToString(Symbol s) {
			return getSymInfo(s).op;
		}

		int symbolPrecedence(Symbol s) {
			return getSymInfo(s).precedence;
		}

		WordToken::WordToken(std::string word)
			: word(word) {}

		TextToken::TextToken(std::string text)
			: text(text) {}

		NumberToken::NumberToken(float number)
			: number(number) {}

		SymbolToken::SymbolToken(Symbol symbol)
			: symbol(symbol) {}

		TokenType TokenSet::type() const {
			return (TokenType)type_int();
		}

		bool TokenSet::isWord() const {
			return type() == TokenType::word;
		}

		bool TokenSet::isWord(std::string word) const {
			return isWord() && getWord() == word;
		}

		bool TokenSet::isText() const {
			return type() == TokenType::text;
		}

		bool TokenSet::isNumber() const {
			return type() == TokenType::number;
		}

		bool TokenSet::isSymbol() const {
			return type() == TokenType::symbol;
		}

		bool TokenSet::isSymbol(Symbol symbol) const {
			return isSymbol() && getSymbol() == symbol;
		}

		std::string TokenSet::getWord() const {
			return ((WordToken*)current())->word;
		}

		std::string TokenSet::getText() const {
			return ((TextToken*)current())->text;
		}

		float TokenSet::getNumber() const {
			return ((NumberToken*)current())->number;
		}

		Symbol TokenSet::getSymbol() const {
			return ((SymbolToken*)current())->symbol;
		}
	}
}
