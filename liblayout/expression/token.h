#ifndef LAYOUT_EXPRESSION_TOKENIZER_H
#define LAYOUT_EXPRESSION_TOKENIZER_H

#include "tokenizerbase.h"

namespace layout {
	namespace expression {
		enum class TokenType {
			word, text, number, symbol
		};

		enum class Symbol {
			none,
			pluss, minus, multiply, divide, modulo,
			parenthesis_open, parenthesis_close,
			comma
		};

		Symbol stringToSymbol(std::string);
		std::string symbolToString(Symbol);
		int symbolPrecedence(Symbol);

		class WordToken : public tokenizer::Token {
		public:
			WordToken(std::string word);
			std::string word;
			virtual int type() { return (int)TokenType::word; }
			virtual std::string to_string() {
				return "{word:'"+word+"'}";
			};
		};

		class TextToken : public tokenizer::Token {
		public:
			TextToken(std::string text);
			std::string text;
			virtual int type() { return (int)TokenType::text; }
			virtual std::string to_string() {
				return "{text:'"+text+"'}";
			};
		};

		class NumberToken : public tokenizer::Token {
			public:
			NumberToken(float number);
			float number;
			virtual int type() { return (int)TokenType::number; }
			virtual std::string to_string() {
				return "{number:"+std::to_string(number)+"}";
			};
		};

		class SymbolToken : public tokenizer::Token {
			public:
			SymbolToken(Symbol symbol);
			Symbol symbol;
			virtual int type() { return (int)TokenType::symbol; }
			virtual std::string to_string() {
				return "{symbol:"+symbolToString(symbol)+"}";
			};
		};

		class TokenSet : public tokenizer::TokenSet {
		public:
			TokenSet(std::string text);

			TokenType type() const;

			bool isWord() const;
			bool isWord(std::string condition) const;
			bool isText() const;
			bool isNumber() const;
			bool isSymbol() const;
			bool isSymbol(Symbol condition) const;

			std::string getWord() const;
			std::string getText() const;
			float getNumber() const;
			Symbol getSymbol() const;
		};
	}
}

#endif
