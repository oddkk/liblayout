#include "layout/expression/functions.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include <cmath>
#include <map>
#include "layout/layout.h"
#include "layout/element.h"

namespace layout {
	namespace expression {
		FunctionArgs::FunctionArgs(const std::vector<Value>& args,
		                           const Context& context)
			: args(args), context(context) {}

		FunctionDefinition::FunctionDefinition()
			: numParams(0), _func([](const FunctionArgs& fa) {return Value();})
			{}

		FunctionDefinition::FunctionDefinition(int numParams, Func func)
			: numParams(numParams), _func(func) {}

		void FunctionDefinition::eval(EvalData& ctx) {
			if (ctx.stack.size() < numParams)
				throw TooFewArgumentsException(name,
				                               ctx.stack.size(),
				                               numParams);
			std::vector<Value> args;
			args.resize(numParams);
			for (int i = 0; i < numParams; i++)
			{
				args[args.size() - i - 1] = ctx.stack.top();
				ctx.stack.pop();
			}
			ctx.stack.push(_func(FunctionArgs(args, ctx.context)));
		}

		typedef FunctionDefinition FuncDef;
		static std::map<std::string, FunctionDefinition> functions;
		static bool functionsInitialized = false;

		void initializeFunctions() {
			if (functionsInitialized) return;
			functionsInitialized = true;
			registerFunction("+",
				 FuncDef(2, [](const FunctionArgs& fa) {
						 return fa.args[0] + fa.args[1];
					 }));
			registerFunction("-",
				 FuncDef(2, [](const FunctionArgs& fa) {
						return fa.args[0] - fa.args[1];
					}));
			registerFunction("*",
				 FuncDef(2, [](const FunctionArgs& fa) {
						return fa.args[0] * fa.args[1];
					}));
			registerFunction("/",
				 FuncDef(2, [](const FunctionArgs& fa) {
						return fa.args[0] / fa.args[1];
					}));
			registerFunction("sin",
				 FuncDef(1, [](const FunctionArgs& fa) {
					 return Value(sinf(fa.args[0].number()));
				 }));
			registerFunction("cos",
				 FuncDef(1, [](const FunctionArgs& fa) {
						 return Value(cosf(fa.args[0].number()));
					 }));
			registerFunction("tan",
				 FuncDef(1, [](const FunctionArgs& fa) {
						 return Value(tanf(fa.args[0].number()));
					 }));
			registerFunction("pi",
				 FuncDef(0, [](const FunctionArgs& fa) {
						return Value((float)M_PI);
					}));
			registerFunction("deg",
				 FuncDef(1, [](const FunctionArgs& fa) {
						return Value(fa.args[0] * (float)M_PI / 180.0f);
					 }));
			registerFunction("rad",
				 FuncDef(1, [](const FunctionArgs& fa) {
						return Value(fa.args[0]);
					 }));
			registerFunction("px",
				 FuncDef(1, [](const FunctionArgs& fa) {
						return Value(fa.args[0]);
					 }));
			registerFunction("vw",
				 FuncDef(1, [](const FunctionArgs& fa) {
						Layout* layout = fa.context.get<Layout*>("layout");
						Vec2 v = layout->viewportSize();
						return Value(fa.args[0] * v.x / 100.0f);
					 }));
			registerFunction("vh",
				 FuncDef(1, [](const FunctionArgs& fa) {
						Layout* layout = fa.context.get<Layout*>("layout");
						Vec2 v = layout->viewportSize();
						return Value(fa.args[0] * v.y / 100.0f);
					}));
			registerFunction("vmin",
				 FuncDef(1, [](const FunctionArgs& fa) {
						Layout* layout = fa.context.get<Layout*>("layout");
						Vec2 v = layout->viewportSize();
						return Value(fa.args[0] *
						             (float)fmin(v.x, v.y) / 100.0f);
					}));
			registerFunction("vmax",
				 FuncDef(1, [](const FunctionArgs& fa) {
						Layout* layout = fa.context.get<Layout*>("layout");
						Vec2 v = layout->viewportSize();
						return Value(fa.args[0] *
						             (float)fmax(v.x, v.y) / 100.0f);
					}));
			registerFunction("pw",
				 FuncDef(1, [](const FunctionArgs& fa) {
						Element* element = fa.context.get<Element*>("element");
						Vec2 p = element->parent()->rectangle.px().size();
						return Value(fa.args[0] * p.x / 100.0f);
					}));
			registerFunction("ph",
				 FuncDef(1, [](const FunctionArgs& fa) {
						Element* element = fa.context.get<Element*>("element");
						Vec2 p = element->parent()->rectangle.px().size();
						return Value(fa.args[0] * p.y / 100.0f);
					}));
			registerFunction("pmin",
				 FuncDef(1, [](const FunctionArgs& fa) {
						Element* element = fa.context.get<Element*>("element");
						Vec2 p = element->parent()->rectangle.px().size();
						return Value(fa.args[0] *
						             (float)fmin(p.x, p.y) / 100.0f);
					}));
			registerFunction("pmax",
				 FuncDef(1, [](const FunctionArgs& fa) {
						Element* element = fa.context.get<Element*>("element");
						Vec2 p = element->parent()->rectangle.px().size();
						return Value(fa.args[0] *
						             (float)fmax(p.x, p.y) / 100.0f);
					}));

			registerFunction("propertyof",
				 FuncDef(2, [](const FunctionArgs& fa) {
						std::string sel = fa.args[0].text();
						std::string property = fa.args[1].text();
						Layout* layout = fa.context.get<Layout*>("layout");
						Element* element = fa.context.get<Element*>("element");
						std::string callingProperty =
							fa.context.get<std::string>("property");
						ElementList res = layout->select(sel);
						if (!res.empty()) {
							if (!(res[0] == element &&
								  callingProperty == property))
								return res[0]->getDeclaration(property).get(fa.context);
						}
						return Value();
					}));
			registerFunction("siblingindex",
				 FuncDef(0, [](const FunctionArgs& fa) {
						Element* element = fa.context.get<Element*>("element");
						return Value((float)element->parent()->children().elementIndex(element));
					}));
		}

		void registerFunction(std::string name, FunctionDefinition def) {
			def.name = name;
			functions.emplace(name, def);
		}


		FunctionDefinition getFunctionDefinition(std::string name) {
			initializeFunctions();
			auto iter = functions.find(name);
			if (iter != functions.end())
				return iter->second;
			throw FunctionDoesNotExistException(name);
		}

		FunctionDoesNotExistException
			::FunctionDoesNotExistException(std::string name)
			: name(name), _what("Function '" + name + "' does not exist") {}

		const char* FunctionDoesNotExistException::what() const throw() {
			return _what.c_str();
		}

		TooFewArgumentsException::
			TooFewArgumentsException(std::string func,
			                         unsigned int numGiven,
			                         unsigned int numExpected)
			: function(func),
			  numGiven(numGiven),
			  numExpected(numExpected) {
				  _what = "Function '" + function + "' called with too few " +
					  "arguments (" + std::to_string(numGiven) + "). " +
					  "Expected " + std::to_string(numExpected);
			}

		const char* TooFewArgumentsException::what() const throw() {
			return _what.c_str();
		}
	}
}

