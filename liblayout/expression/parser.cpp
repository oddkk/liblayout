#include "layout/expression/expression.h"
#include "layout/expression/functions.h"
#include "expression/token.h"
#include <vector>
#include <stack>

namespace layout
{
	namespace expression
	{
		typedef tokenizer::Token Token;
		typedef std::vector<Statement*> StatementList;
		typedef std::stack<Token*> OperatorStack;

		static bool isSymbol(Token* token, Symbol sym=Symbol::none) {
			if (!token) return false;
			return (TokenType)token->type() == TokenType::symbol &&
					(((SymbolToken*)token)->symbol == sym ||
					 sym == Symbol::none);
		}

		static bool isWord(Token* token, std::string word="") {
			if (!token) return false;
			return (TokenType)token->type() == TokenType::word &&
					(((WordToken*)token)->word == word || word == "");
		}

		static bool isNumber(Token* token) {
			if (!token) return false;
			return (TokenType)token->type() == TokenType::number;
		}

		static Function* functionFromToken(Token* t) {
			if (isSymbol(t))
				return new Function(symbolToString(((SymbolToken*)t)->symbol));
			else if (isWord(t))
				return new Function(((WordToken*)t)->word);
			throw tokenizer::InvalidTokenException(t);
		}

		static int precedenceFromToken(Token* t) {
			if (isSymbol(t))
				return symbolPrecedence(((SymbolToken*)t)->symbol);
			else if (isWord(t))
				return 2;
			throw tokenizer::InvalidTokenException(t);
		}

		static void popUntilSymbolOnTop(std::stack<Token*>& opStack,
		                                std::vector<Statement*>& stmts,
		                                Symbol sym) {
			while (!opStack.empty() &&
					!isSymbol(opStack.top(), sym)) {
				stmts.push_back((Statement*)functionFromToken(opStack.top()));
				opStack.pop();
			}
		}

		static void parsePostfix(TokenSet& ts,
		                         StatementList& stmts,
		                         OperatorStack&) {
			if (getFunctionDefinition(ts.getWord()).numParams != 1)
				throw PostfixFunctionTooManyParamsException(ts.getWord());
			stmts.push_back(new Function(ts.getWord()));
		}

		static void parseNumber(TokenSet& ts,
		                        StatementList& stmts,
		                        OperatorStack&) {
			stmts.push_back(new Litteral(Value(ts.getNumber())));
		}

		static void parseText(TokenSet& ts,
		                      StatementList& stmts,
		                      OperatorStack&) {
			stmts.push_back(new Litteral(Value(ts.getText())));
		}

		static void parseNone(TokenSet& ts,
		                      StatementList& stmts,
		                      OperatorStack&) {
			stmts.push_back(new Litteral(Value()));
		}

		static void parseBoolean(TokenSet& ts,
		                         StatementList& stmts,
		                         OperatorStack&) {
			stmts.push_back(new Litteral(Value(ts.getWord() == "true")));
		}

		static void parseArgumentSeparator(TokenSet& ts,
		                                   StatementList& stmts,
		                                   OperatorStack& opStack) {
			popUntilSymbolOnTop(opStack, stmts, Symbol::parenthesis_open);
			if (opStack.empty())
				throw ParenthesisMismatchException();
		}

		static void parseParenthesisClose(TokenSet& ts,
		                                  StatementList& stmts,
		                                  OperatorStack& opStack) {
			popUntilSymbolOnTop(opStack, stmts, Symbol::parenthesis_open);
			if (opStack.empty())
				throw ParenthesisMismatchException();
			opStack.pop();
			if (!opStack.empty() && isWord(opStack.top())) {
				stmts.push_back(functionFromToken(opStack.top()));
				opStack.pop();
			}
		}

		static void parseParenthesisOpen(TokenSet& ts,
		                                 StatementList& stmts,
		                                 OperatorStack& opStack) {
			opStack.push(ts.current());
		}

		static void parseSymbol(TokenSet& ts,
		                        StatementList& stmts,
		                        OperatorStack& opStack) {
			while (!opStack.empty() &&
					precedenceFromToken(ts.current()) <=
					precedenceFromToken(opStack.top())) {
				stmts.push_back(functionFromToken(opStack.top()));
				opStack.pop();
			}
			opStack.push(ts.current());
		}

		static void parseWord(TokenSet& ts,
		                      StatementList& stmts,
		                      OperatorStack& opStack) {
			opStack.push(ts.current());
		}

		static struct {
			std::string name;
			void (*parse)(TokenSet&, StatementList&, OperatorStack&);
			bool (*condition)(const TokenSet&);
		} parseMethods[] = {
			{ "postfix", parsePostfix,
				[](const TokenSet& ts) -> bool {
					return isNumber(ts.peek_previous()) && ts.isWord();
				} },
			{ "number", parseNumber,
				[](const TokenSet& ts) -> bool {
					return ts.isNumber();
				} },
			{ "text", parseText,
				[](const TokenSet& ts) -> bool {
					return ts.isText();
				} },
			{ "none", parseNone,
				[](const TokenSet& ts) -> bool {
					return ts.isWord("none");
				} },
			{ "boolean", parseBoolean,
				[](const TokenSet& ts) -> bool {
					return ts.isWord("true") || ts.isWord("false");
				} },
			{ "argument separator", parseArgumentSeparator,
				[](const TokenSet& ts) -> bool {
					return ts.isSymbol(Symbol::comma);
				} },
			{ "parenthesis close", parseParenthesisClose,
				[](const TokenSet& ts) -> bool {
					return ts.isSymbol(Symbol::parenthesis_close);
				} },
			{ "parenthesis open", parseParenthesisOpen,
				[](const TokenSet& ts) -> bool {
					return ts.isSymbol(Symbol::parenthesis_open);
				} },
			{ "symbol", parseSymbol,
				[](const TokenSet& ts) -> bool {
					return ts.isSymbol();
				} },
			{ "word", parseWord,
				[](const TokenSet& ts) -> bool {
					return ts.isWord();
				} },
		};

		static std::vector<Statement*> parseTokens(TokenSet& ts) {
			// Implementation of the Shunting-yard algorithm
			StatementList stmts;
			OperatorStack opStack;
			while (!ts.eos()) {
				for (int i = 0;
				     i < sizeof(parseMethods) / sizeof(parseMethods[0]); i++) {
					if (parseMethods[i].condition(ts)) {
						parseMethods[i].parse(ts, stmts, opStack);
						break;
					}
				}
				ts.next();
			}
			while(!opStack.empty()) {
				stmts.push_back(functionFromToken(opStack.top()));
				opStack.pop();
			}
			return stmts;
		}

		Expression::Expression(const std::string& expr)
			: text(expr) {
			expression::TokenSet tokens (expr);
			std::vector<Statement*> stmts = parseTokens(tokens);
			for (unsigned int i = 0; i < stmts.size(); i++)
				_stmts.push_back(std::unique_ptr<Statement>(stmts[i]));
		}
	}
}
