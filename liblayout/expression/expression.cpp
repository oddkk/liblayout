#include "layout/expression/expression.h"
#include "layout/expression/functions.h"

namespace layout
{
	namespace expression
	{
		Value::Value()
			: _type(ValueType::none),
			_text(), _number(), _boolean() {}

		Value::Value(std::string text)
			: _type(ValueType::text),
			_text(text), _number(), _boolean() {}

		Value::Value(float number)
			: _type(ValueType::number),
			_text(), _number(number), _boolean() {}

		Value::Value(bool boolean)
			: _type(ValueType::boolean),
			_text(), _number(), _boolean(boolean) {}

		ValueType Value::type() const { return _type; }
		std::string Value::text() const {
			switch (_type) {
			case ValueType::none:
				return "";
			case ValueType::text:
				return _text;
			case ValueType::number:
				return std::to_string(_number);
			case ValueType::boolean:
				return _boolean ? "true" : "false";
			}
		}
		float Value::number() const {
			switch (_type) {
			case ValueType::none:
				return 0.0f;
			case ValueType::text:
				return std::atof(_text.c_str());
			case ValueType::number:
				return _number;
			case ValueType::boolean:
				return _boolean ? 1.0f : 0.0f;
			}
		}
		bool Value::boolean() const {
			switch (_type) {
			case ValueType::none:
				return false;
			case ValueType::text:
				return _text == "true";
			case ValueType::number:
					return _number == 0.0f;
			case ValueType::boolean:
				return _boolean;
			}
		}

		bool Value::none() const {
			return _type == ValueType::none;
		}

		void Value::text(std::string text) {
			_type = ValueType::text;
			_text = text;
			_number = 0.0f;
			_boolean = false;
		}

		void Value::number(float number) {
			_type = ValueType::number;
			_text = "";
			_number = number;
			_boolean = false;
		}

		void Value::boolean(bool boolean) {
			_type = ValueType::boolean;
			_text = "";
			_number = 0.0f;
			_boolean = boolean;
		}

		void Value::setNone() {
			_type = ValueType::none;
			_text = "";
			_number = 0.0f;
			_boolean = false;
		}

		Value Value::operator+(const Value& rhs) const {
			if (type() == ValueType::text || rhs.type() == ValueType::text)
				return Value(text() + rhs.text());
			else return Value(number() + rhs.number());
		}

		Value Value::operator-(const Value& rhs) const {
			if (type() == ValueType::text || rhs.type() == ValueType::text)
				throw InvalidOperatorException(*this, rhs, "-");
			else return Value(number() - rhs.number());
		}

		Value Value::operator*(const Value& rhs) const {
			if (type() == ValueType::text || rhs.type() == ValueType::text)
				throw InvalidOperatorException(*this, rhs, "*");
			else return Value(number() * rhs.number());
		}

		Value Value::operator/(const Value& rhs) const {
			if (type() == ValueType::text || rhs.type() == ValueType::text)
				throw InvalidOperatorException(*this, rhs, "/");
			else return Value(number() / rhs.number());
		}

		std::string Value::to_string() const {
			std::string type;
			switch (_type) {
			case ValueType::none: type = "none"; break;
			case ValueType::text: type = "text"; break;
			case ValueType::number: type = "number"; break;
			case ValueType::boolean: type = "boolean"; break;
			}
			return "{" + type + ":" + text() + "}";
		}

		EvalData::EvalData(const Context& ctx)
			: context(ctx) {}

		Litteral::Litteral(Value value)
			: value(value) {}

		void Litteral::eval(EvalData& context) const {
			context.stack.push(value);
		}

		std::string Litteral::to_string() const {
			return value.to_string();
		}

		Function::Function(std::string name)
			: name(name) {}

		unsigned int Function::numParams() const {
			return getFunctionDefinition(name).numParams;
		}

		void Function::eval(EvalData& context) const {
			FunctionDefinition fd = getFunctionDefinition(name);
			fd.eval(context);
		}

		std::string Function::to_string() const {
			return "[" + name + "]";
		}

		Expression::Expression(const Expression& e)
			: text(e.text)
		{
			for (unsigned int i = 0; i < e._stmts.size(); i++) {
				_stmts.push_back(
						std::unique_ptr<Statement>(e._stmts[i]->clone()));
			}
		}

		Expression& Expression::operator=(const Expression& expr) {
			text = expr.text;
			_stmts.clear();
			for (auto& stmt : expr._stmts)
				_stmts.push_back(std::unique_ptr<Statement>(stmt->clone()));
			return *this;
		}

		Value Expression::eval(const Context& context) const {
			EvalData ctx (context);
			for (unsigned int i = 0; i < _stmts.size(); i++)
				_stmts[i]->eval(ctx);
			if (!ctx.stack.empty())
				return ctx.stack.top();
			else
				return Value();
		}

		std::string Expression::to_string() const {
			std::string res = "";
			for (unsigned int i = 0; i < _stmts.size(); i++)
				res += _stmts[i]->to_string();
			return res;
		}

		InvalidOperatorException::InvalidOperatorException(const Value& lhs,
			                                               const Value& rhs,
			                                               std::string op)
			: lhs(lhs), rhs(rhs), op(op) {
			_what = "Invalid use of operator '" + op + "' on values (" +
				lhs.to_string() + op + rhs.to_string() + ")";
		}
		const char* InvalidOperatorException::what() const throw() {
			return _what.c_str();
		}

		ParenthesisMismatchException::ParenthesisMismatchException() {
			_what = "Parenthesis mismatch";
		}
		const char* ParenthesisMismatchException::what() const throw() {
			return _what.c_str();
		}

		PostfixFunctionTooManyParamsException::
			PostfixFunctionTooManyParamsException(std::string func)
			: functionName(func) {
			_what = "Cannot use function '" + func + "' postfix." +
				"The function takes " +
				std::to_string(getFunctionDefinition(func).numParams) +
				" parameters, but postfix functions can only take 1.";
		}
		const char* PostfixFunctionTooManyParamsException::
			what() const throw() {
			return _what.c_str();
		}
	}
}

