#include "layout/declaration.h"
#include "layout/layout.h"
#include "strutils.h"
#include "utf8.h"
#include <algorithm>

#include <iostream>

namespace layout {
	Declaration::Declaration()
		: _property(),
		  _selector(),
		  _expr("")
	{}

	Declaration::Declaration(std::string decl, Selector sel)
		: _property(),
		  _selector(),
		  _expr("") {
		str::iterator iter = decl.begin();
		std::string property = str::readUntil(iter, decl.end(), ':');
		if (iter != decl.end()) utf8::next(iter, decl.end());
		std::string value = str::readUntil(iter, decl.end(), ';');
		if (iter != decl.end()) utf8::next(iter, decl.end());

		property = str::trimWhitespace(property);
		value = str::trimWhitespace(value);

		_property = property;
		_selector = sel;
		_expr = expression::Expression(value);
	}

	Declaration::Declaration(std::string property,
	                         Selector sel,
	                         expression::Expression expr)
		: _property(property),
		  _selector(sel),
		  _expr(expr)
	{}

	Selector Declaration::selector() const {
		return _selector;
	}

	std::string Declaration::property() const {
		return _property;
	}

	expression::Value Declaration::get(const Context& ctx) const {
		Context c = ctx;
		c.set<std::string>("property", _property);
		return _expr.eval(c);
	}

	DeclarationList::DeclarationList(IDeclarationContainer* owner,
		                             std::string name)
		: _declarations(),
		  _owner(owner),
		  _name(name)
	{}

	bool compareDeclaration(Declaration lhs,
	                        Declaration rhs) {
		return lhs.selector().score() < rhs.selector().score();
	}

	void DeclarationList::apply(Declaration d) {
		_declarations.push_front(d);
		_declarations.sort(compareDeclaration);
	}

	void DeclarationList::apply(const DeclarationList& list) {
		for (const auto& decl : list._declarations)
			apply(decl);
	}

	Declaration DeclarationList::get() const {
		for (auto iter = _declarations.rbegin();
		     iter != _declarations.rend(); iter++) {
			if (_owner->_matchesSelector(iter->selector()))
				return *iter;
		}
		return Declaration(_name,
		                   Selector(""),
		                   expression::Expression("none"));
	}

	void DeclarationList::clear() {
		_declarations.clear();
	}


	void IDeclarationContainer::applyDeclaration(const Declaration& dec) {
		_getDeclList(dec.property())->apply(dec);
	}

	Declaration IDeclarationContainer::getDeclaration(std::string property) {
		return _getDeclList(property)->get();
	}

	void IDeclarationContainer::clearDeclarations() {
		for (auto& iter : _declarations) {
			iter.second->clear();
		}
	}

	DeclarationList* IDeclarationContainer::_getDeclList(std::string property) {
		auto& decl = _declarations[property];
		if (!decl)
			decl = std::unique_ptr<DeclarationList>
				(new DeclarationList(this, property));
		return decl.get();
	}

}
