#include "layout/event.h"

namespace layout {
	Event::Event(std::string name,
		         IEventReceiver* target,
		         IEventReceiver* sender)
		: name(name),
		  target(target),
		  sender(sender),
		  stopPropagation(false)
	{}

	void IEventReceiver::raise(std::string eventName) {
		raise(Event(eventName, this, this));
	}

	void IEventReceiver::raise(Event event) {
		Event e = Event(event.name, event.target, this);
		raiseCallback(e.name, e);
		if (e.stopPropagation)
			return;
		IEventReceiver* parent = _getParentEventReceiver();
		if (parent)
			parent->raise(event);
	}

	void IEventReceiver::on(EventCallback callback) {
		bindCallback(callback);
	}

	void IEventReceiver::on(std::string event, EventCallbackFunction function) {
		bindCallback(event, function);
	}
}

