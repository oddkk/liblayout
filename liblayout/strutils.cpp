#include "strutils.h"
#include "utf8.h"
#include <algorithm>
#include <functional>
#include <cctype>
#include <locale>

namespace layout {
	namespace str {
		std::vector<std::string> split(
				std::string str, unsigned int c) {
			std::vector<std::string> result;
			iterator it = str.begin();
			iterator end = str.end();
			while (it != end) {
				result.push_back(readUntil(it, end, c));
				if (it != end)
					utf8::next(it, end);
			}
			return result;
		}

		std::string readWhile(
				iterator& it,
				const iterator& end,
				bool (*func)(unsigned int)) {
			std::string result;
			while (it != end) {
				if (!func(utf8::peek_next(it, end)))
					break;
				utf8::append(utf8::next(it, end), std::back_inserter(result));
			}
			return result;
		}
		std::string readUntil(
				iterator& it,
				const iterator& end,
				unsigned int c) {
			std::string result;
			while (it != end) {
				if (utf8::peek_next(it, end) == c)
					break;
				utf8::append(utf8::next(it, end), std::back_inserter(result));
			}
			return result;
		}

		void trimWhitespace(
				iterator& it,
				const iterator& end) {
			readWhile(it, end, isWhitespace);
		}

		std::string trimWhitespace(std::string str) {
			str.erase(str.begin(), std::find_if(str.begin(), str.end(),
			          std::not1(std::ptr_fun<int, int>(std::isspace))));
			str.erase(std::find_if(str.rbegin(),
			                       str.rend(),
			                       std::not1(
			                          std::ptr_fun<int, int>(
			                              std::isspace
			                          )
			                       )
			          ).base(),
			          str.end());
			return str;
		}

		bool isWhitespace(unsigned int c) {
			return isspace(c);
		}

		bool isNumber(unsigned int c) {
			return isdigit(c) || c == '.';
		}

		bool isInteger(unsigned int c) {
			return isdigit(c);
		}

		bool isAlphabetic(unsigned int c) {
			return isalpha(c);
		}

		bool isWord(unsigned int c) {
			return isalpha(c) || isdigit(c);
		}

		bool isSymbol(unsigned int c) {
			return ispunct(c);
		}

		bool isText(unsigned int c) {
			return c == '"' || c == '\'';
		}

	}
}

