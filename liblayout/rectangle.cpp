#include "layout/rectangle.h"
#include "layout/layout.h"

#include <iostream>

namespace layout {
	struct Axis {
		Axis() : pos(0), scalar(0) {}
		Axis(float pos, float scalar)
			: pos(pos), scalar(scalar)
		{}
		float pos, scalar;
	};
	static Axis calculateAxis(const Property& side1,
	                          const Property& side2,
	                          const Property& length,
	                          const Property& center,
	                          float parentLength) {
		Axis res(0.0f, 1.0f);
		bool length_set = !length.get().none();
		bool center_set = !center.get().none();
		bool side1_set = !side1.get().none();
		bool side2_set = !side2.get().none();
		float s1 = ( (float)side1 / parentLength - 0.5f);
		float s2 = (-(float)side2 / parentLength + 0.5f);

		if (length_set)
			res.scalar = (float)length / parentLength;

		if (side1_set && side2_set) {
			res.scalar = s2 - s1;
			res.pos = s1 + res.scalar / 2.0f;
		}
		else if (side1_set) {
			res.pos = s1 + res.scalar / 2.0f;
		}
		else if (side2_set) {
			res.pos = s2 - res.scalar / 2.0f;
		}

		if (center_set)
			res.pos = (float)center / parentLength;

		return res;
	}

	Box Rectangle::px() const {
		Box u = unit();
		Vec2 parent = parentBoxPx().size();
		return Box::fromCenter(u.center() * parent, u.size() * parent);
	}

	Box Rectangle::unit() const {
		Box parent = parentBoxPx();

		Axis xAxis = calculateAxis(left, right, width, x, parent.width());
		Axis yAxis = calculateAxis(top, bottom, height, y, parent.height());

		return Box::fromCenter(xAxis.pos, yAxis.pos, xAxis.scalar, yAxis.scalar);
	}

	Mat33 Rectangle::transformation() const {
		Box u = unit();
		Mat33 mat;
		mat  = Mat33::translate(u.center());
		mat *= Mat33::rotate((float)rotation);
		mat *= Mat33::scale(u.size());
		return mat;
	}

	Mat33 Rectangle::fromScreenTransformation() const {
		return Mat33::identity();
	}

	void Rectangle::bind(Element* e) {
		_owner = e;
		left.bind(e, "left");
		right.bind(e, "right");
		top.bind(e, "top");
		bottom.bind(e, "bottom");
		x.bind(e, "x");
		y.bind(e, "y");
		width.bind(e, "width");
		height.bind(e, "height");
		rotation.bind(e, "rotation");
	}

	Element* Rectangle::owner() const {
		return _owner;
	}

	Box Rectangle::parentBoxPx() const {
		if (owner()->parent())
			return owner()->parent()->rectangle.px();
		else
			return Box::fromCenter(Vec2(), owner()->layout()->viewportSize());
	}

	Box Rectangle::parentBoxUnit() const {
		if (owner()->parent())
			return owner()->parent()->rectangle.unit();
		else
			return Box::fromCenter(Vec2(), Vec2(1, 1));
	}
}

