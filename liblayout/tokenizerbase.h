#ifndef TOKENIZER_H
#define TOKENIZER_H

#include <string>
#include <vector>
#include <memory>

namespace layout {
	namespace tokenizer {
		class Token {
		public:
			virtual int type()=0;
			virtual std::string to_string()=0;
		};

		struct TokenizerMethod {
			std::string name;
			void (*tokenize)(
					unsigned int,
					std::string::iterator&,
					const std::string::iterator&,
					std::vector<std::unique_ptr<Token>>&);
			bool (*condition)(unsigned int);
		};

		class TokenSet {
		public:
			TokenSet(std::string text,
					TokenizerMethod* methods,
					unsigned int methodsCount);
			std::vector<std::unique_ptr<Token>> tokens;
			unsigned int index;

			void reset();
			void next();

			Token* current() const;
			Token* peek_previous() const;
			Token* peek_next() const;
			int type_int() const;
			bool eos() const;

			std::string to_string() const;
		};

		class InvalidTokenException : public std::exception {
		public:
			InvalidTokenException(Token*);

			virtual const char* what() const throw();
		private:
			std::string _what;
		};
	}
}

#endif
