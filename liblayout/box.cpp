#include "layout/box.h"

namespace layout {
	Box::Box()
		: _topLeft(), _size() {}

	Box::Box(Vec2 topLeft, Vec2 size)
		: _topLeft(topLeft), _size(size) {}

	Box::Box(float left, float top, float width, float height)
		: _topLeft(left, top), _size(width, height) {}

	float Box::top() const {
		return _topLeft.y;
	}

	float Box::bottom() const {
		return _topLeft.y + _size.y;
	}

	float Box::left() const {
		return _topLeft.x;
	}

	float Box::right() const {
		return _topLeft.x + _size.x;
	}

	Vec2 Box::topLeft() const {
		return _topLeft;
	}

	Vec2 Box::center() const {
		return _topLeft + Vec2(_size.x / 2, _size.y / 2);
	}

	Vec2 Box::size() const {
		return _size;
	}

	float Box::width() const {
		return _size.x;
	}

	float Box::height() const {
		return _size.y;
	}

	void Box::top(float v) {
		_topLeft.y = v;
	}

	void Box::bottom(float v, float pheight) {
		_topLeft.y = pheight - v - _size.y;
	}

	void Box::left(float v) {
		_topLeft.x = v;
	}

	void Box::right(float v, float pwidth) {
		_topLeft.x = pwidth - v - _size.x;
	}

	void Box::center(Vec2 pos) {
		_topLeft = pos - Vec2(_size.x / 2, _size.y / 2);
	}

	void Box::size(Vec2 size) {
		_size = size;
	}

	void Box::width(float v) {
		_size.x = v;
	}

	void Box::height(float v) {
		_size.y = v;
	}

	Box Box::fromSides(float left, float top, float right, float bottom) {
		return Box(left, top, right - left, bottom - top);
	}

	Box Box::fromCenter(float x, float y, float width, float height) {
		return Box(Vec2(x, y) - Vec2(width/2.0f, height/2.0f), Vec2(width, height));
	}

	Box Box::fromCenter(Vec2 pos, Vec2 size) {
		return fromCenter(pos.x, pos.y, size.x, size.y);
	}
}

