#include "layout/layout.h"
#include "rapidxml/rapidxml.hpp"

using namespace rapidxml;

namespace layout {
	void parseElementParameter(Element* element, xml_node<>* node) {
		for (xml_attribute<>* attr = node->first_attribute();
				attr; attr = attr->next_attribute()) {
			std::string attr_name = attr->name();
			if (attr_name == "name")
				element->name = attr->value();
			else if (attr_name == "class")
				element->styleClass.activate(attr->value());
			else
				element->applyDeclaration(
						Declaration(
							attr_name,
							Selector(),
							expression::Expression(attr->value())));
		}
	}

	Element* constructElement(xml_node<>* node) {
		Element* element = Element::construct(node->name());
		return element;
	}

	void giveInnerText(Element* element, xml_node<>* node) {
		element->innerText(std::string(node->value(),
		                               node->value_size()));
	}

	void parseElementChildren(Element* element, xml_node<>* node) {
		if (!element->requestInnerText()) {
			for (xml_node<>* child = node->first_node();
					child; child = child->next_sibling()) {
				Element* c = constructElement(child);
				if (!c) continue;
				if (c->requestInnerText())
					giveInnerText(c, child);
				element->appendChild(c);
				parseElementParameter(c, child);
				parseElementChildren(c, child);
			}
		}
	}

	void Layout::load(std::string data) {
		xml_document<> doc;
		doc.parse<0>(const_cast<char*>(data.c_str()));
		xml_node<>* root = doc.first_node("layout");
		if (!root)
			throw RootNodeNotFoundException();
		parseElementParameter(_root.get(), root);
		parseElementChildren(_root.get(), root);
	}

	const char* RootNodeNotFoundException::what() const throw() {
		return "Root node not found. Expected 'layout' node.";
	}
}

