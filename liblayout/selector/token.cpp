#include "selector/token.h"

namespace layout
{
	namespace selector
	{
		static const struct _sym_info {
			Symbol symbol;
			std::string op;
		} symbols[] = {
			{ Symbol::none,				" "  },
			{ Symbol::pluss,			"+"  },
			{ Symbol::minus,			"-"  },
			{ Symbol::child_sign,		">"  },
			{ Symbol::parent_sign,		"<"  },
			{ Symbol::sibling_sign,		"~"  },
			{ Symbol::name_sign,		"#"  },
			{ Symbol::class_sign,		"."  },
			{ Symbol::pseudo_sign,		":"  },
			{ Symbol::self,				"&"  },
		};
		static const size_t symbols_size = sizeof(symbols) /
		                                   sizeof(symbols[0]);

		static _sym_info getSymInfo(Symbol sym) {
			for (size_t i = 0; i < symbols_size; i++) {
				if (symbols[i].symbol == sym)
					return symbols[i];
			}
			return symbols[0];
		}

		Symbol stringToSymbol(std::string str) {
			for (size_t i = 0; i < symbols_size; i++) {
				if (symbols[i].op == str)
					return symbols[i].symbol;
			}
			return Symbol::none;
		}

		std::string symbolToString(Symbol s) {
			return getSymInfo(s).op;
		}

		WordToken::WordToken(std::string word)
			: word(word) {}

		NumberToken::NumberToken(float number)
			: number(number) {}

		SymbolToken::SymbolToken(Symbol symbol)
			: symbol(symbol) {}

		TokenType TokenSet::type() const {
			return (TokenType)type_int();
		}

		bool TokenSet::isWord() const {
			return type() == TokenType::word;
		}

		bool TokenSet::isWord(std::string word) const {
			return isWord() && getWord() == word;
		}

		bool TokenSet::isNumber() const {
			return type() == TokenType::number;
		}

		bool TokenSet::isSymbol() const {
			return type() == TokenType::symbol;
		}

		bool TokenSet::isSymbol(Symbol symbol) const {
			return isSymbol() && getSymbol() == symbol;
		}

		std::string TokenSet::getWord() const {
			return ((WordToken*)current())->word;
		}

		int TokenSet::getNumber() const {
			return ((NumberToken*)current())->number;
		}

		Symbol TokenSet::getSymbol() const {
			return ((SymbolToken*)current())->symbol;
		}
	}
}
