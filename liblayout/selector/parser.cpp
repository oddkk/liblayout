#include "layout/selector.h"
#include "selector/token.h"

#include <string>
#include <vector>
#include <iostream>

using namespace layout::selector;

namespace layout {
	struct SelectorItemPrototype {
		SelectorItemPrototype()
			: op(SelectorOperator::decendent),
			  tag(), name(), classes(),
			  pseudo(), offset(0), self(false) {}
		SelectorOperator op;
		std::string tag;
		std::string name;
		ClassList classes;
		ClassList pseudo;
		int offset;
		bool self;
	};

	static SelectorOperator operatorFromSymbol(Symbol s) {
		if (s == Symbol::child_sign) return SelectorOperator::child;
		if (s == Symbol::parent_sign) return SelectorOperator::parent;
		if (s == Symbol::sibling_sign) return SelectorOperator::sibling;
		return SelectorOperator::none;
	}

	static void parseTag(TokenSet& ts, SelectorItemPrototype& pt) {
		pt.tag = ts.getWord();
	}

	static void parseOperator(TokenSet& ts, SelectorItemPrototype& pt) {
		pt.op = operatorFromSymbol(ts.getSymbol());
	}

	static void parseName(TokenSet& ts, SelectorItemPrototype& pt) {
		ts.next();
		pt.name = ts.getWord();
	}

	static void parseClass(TokenSet& ts, SelectorItemPrototype& pt) {
		ts.next();
		pt.classes.activate(ts.getWord());
	}

	static void parsePseudo(TokenSet& ts, SelectorItemPrototype& pt) {
		ts.next();
		pt.pseudo.activate(ts.getWord());
	}

	static void parseOffset(TokenSet& ts, SelectorItemPrototype& pt) {
		bool positive = ts.getSymbol() == Symbol::pluss;
		ts.next();
		pt.offset = ts.getNumber() * (positive ? 1 : -1);
	}

	static void parseSelf(TokenSet& ts, SelectorItemPrototype& pt) {
		pt.self = true;
	}

	struct {
		std::string name;
		void (*parse)(TokenSet& ts, SelectorItemPrototype&);
		bool (*condition)(const TokenSet& ts);
	} parserMethods[] = {
		{ "operator", parseOperator,
			[](const TokenSet& ts) -> bool {
				return ts.isSymbol() &&
				       operatorFromSymbol(ts.getSymbol()) !=
				           SelectorOperator::none;
			} },
		{ "tag", parseTag,
			[](const TokenSet& ts) -> bool {
				return ts.isWord();
			} },
		{ "name", parseName,
			[](const TokenSet& ts) -> bool {
				return ts.isSymbol(Symbol::name_sign);
			} },
		{ "class", parseClass,
			[](const TokenSet& ts) -> bool {
				return ts.isSymbol(Symbol::class_sign);
			} },
		{ "pseudo", parsePseudo,
			[](const TokenSet& ts) -> bool {
				return ts.isSymbol(Symbol::pseudo_sign);
			} },
		{ "offset", parseOffset,
			[](const TokenSet& ts) -> bool {
				return ts.isSymbol(Symbol::pluss) || ts.isSymbol(Symbol::minus);
			} },
		{ "self", parseSelf,
			[](const TokenSet& ts) -> bool {
				return ts.isSymbol(Symbol::self);
			} },
	};

	SelectorItem::SelectorItem(std::string desc)
			: _tag(), _name(), _styleClass(),
			  _pseudoClass(), _offset(0), _self(false),
			  _operator(SelectorOperator::none) {
		TokenSet ts (desc);
		SelectorItemPrototype pt;

		while (!ts.eos()) {
			for (int i = 0;
			     i < sizeof(parserMethods) / sizeof(parserMethods[0]); i++) {
				if (parserMethods[i].condition(ts)) {
					parserMethods[i].parse(ts, pt);
					break;
				}
			}
			ts.next();
		}

		_tag = pt.tag;
		_name = pt.name;
		_styleClass = pt.classes;
		_pseudoClass = pt.pseudo;
		_offset = pt.offset;
		_operator = pt.op;
		_self = pt.self;
	}
}
