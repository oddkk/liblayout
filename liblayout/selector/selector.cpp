#include "layout/selector.h"
#include "strutils.h"

namespace layout {
	Selector::Selector()
		: items() {}

	static bool isOperator(std::string s) {
		return
			s == ">" ||
			s == "<" ||
			s == "~";
	}

	static std::string splitItem(str::iterator& it, const str::iterator& end) {
		str::trimWhitespace(it, end);
		std::string desc = str::readWhile(it, end,
				[](unsigned int c) -> bool {
					return !str::isWhitespace(c);
				});
		if (isOperator(desc)) {
			str::trimWhitespace(it, end);
			desc += str::readUntil(it, end, ' ');
		}
		return desc;
	}

	Selector::Selector(std::string desc)
		: items() {
		str::iterator it = desc.begin();
		str::iterator end = desc.end();
		while (it != end) {
			items.push_back(SelectorItem(splitItem(it, end)));
		}
	}

	std::string Selector::to_string() const {
		std::string res;
		for (unsigned int i = 0; i < items.size(); i++)
			res += items[i].to_string();
		return res;
	}

	int Selector::score() const {
		if (items.empty())
			return 0;
		return items.back().score();
	}

	Selector Selector::subsection(int begin, int end) const {
		Selector sel;
		sel.items =
			std::vector<SelectorItem>(
				items.begin() + begin,
				items.begin() + end);
		return sel;
	}

	bool operator==(const Selector& lhs, const Selector& rhs) {
		if (lhs.items.size() != rhs.items.size())
			return false;
		for (unsigned int i = 0; i < lhs.items.size(); i++) {
			if (lhs.items[i] != rhs.items[i])
				return false;
		}
		return true;
	}

	bool operator!=(const Selector& lhs, const Selector& rhs) {
		return !(lhs == rhs);
	}
}

