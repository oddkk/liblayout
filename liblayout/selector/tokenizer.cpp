#include "selector/token.h"
#include "strutils.h"
#include "utf8.h"
#include <cctype>

namespace layout {
	namespace selector {
		static Symbol readSymbol(
				std::string::iterator& it,
				const std::string::iterator& end) {
			std::string result;
			unsigned int c = utf8::next(it, end);
			utf8::append(c, std::back_inserter(result));
			return stringToSymbol(result);
		}

		static void tokenizeNumber(
				unsigned int,
				std::string::iterator& it,
				const std::string::iterator& end,
				std::vector<std::unique_ptr<tokenizer::Token>>& tokens) {
			std::string word = str::readWhile(it, end, str::isInteger);
			tokens.push_back(
					std::unique_ptr<tokenizer::Token>(
						(tokenizer::Token*)new NumberToken(atoi(word.c_str()))
						)
					);
		}

		static void tokenizeWord(
				unsigned int,
				std::string::iterator& it,
				const std::string::iterator& end,
				std::vector<std::unique_ptr<tokenizer::Token>>& tokens) {
			std::string word = str::readWhile(it, end, str::isWord);
			tokens.push_back(
					std::unique_ptr<tokenizer::Token>(
						(tokenizer::Token*)new WordToken(word)
						)
					);
		}

		static void tokenizeSymbol(
				unsigned int c,
				std::string::iterator& it,
				const std::string::iterator& end,
				std::vector<std::unique_ptr<tokenizer::Token>>& tokens) {
			Symbol sym = readSymbol(it, end);
			tokens.push_back(
					std::unique_ptr<tokenizer::Token>(
						(tokenizer::Token*)new SymbolToken(sym)
						)
					);
		}

		tokenizer::TokenizerMethod tokenizerMethods[] = {
			{ "number", tokenizeNumber, str::isInteger },
			{ "word", tokenizeWord, str::isAlphabetic },
			{ "symbol", tokenizeSymbol, str::isSymbol },
		};

		TokenSet::TokenSet(std::string text)
			: tokenizer::TokenSet(text,
					tokenizerMethods,
					sizeof(tokenizerMethods) / sizeof(tokenizerMethods[0])) {
		}
	}
}

