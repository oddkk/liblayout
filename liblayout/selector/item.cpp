#include "layout/selector.h"

namespace layout {
	SelectorItem::SelectorItem()
		: _tag(), _name(), _styleClass(),
		  _pseudoClass(), _offset(0), _self(false),
		  _operator(SelectorOperator::none) {}

	std::string SelectorItem::tag() const {
		return _tag;
	}

	std::string SelectorItem::name() const {
		return _name;
	}

	const ClassList& SelectorItem::styleClass() const {
		return _styleClass;
	}

	const ClassList& SelectorItem::pseudoClass() const {
		return _pseudoClass;
	}

	int SelectorItem::offset() const {
		return _offset;
	}

	bool SelectorItem::self() const {
		return _self;
	}

	SelectorOperator SelectorItem::op() const {
		return _operator;
	}

	std::string SelectorItem::to_string() const {
		std::string str;
		if (_operator == SelectorOperator::child)
			str += ">";
		else if (_operator == SelectorOperator::sibling)
			str += "~";
		if (_self) str += "&";
		if (_tag != "") str += _tag;
		if (_name != "") str += "#" + _name;
		str += _styleClass.to_string(".");
		str += _pseudoClass.to_string(":");
		if (_offset < 0)
			str += "-" + std::to_string(_offset * -1);
		else if (_offset > 0)
			str += "+" + std::to_string(_offset);
		return str;
	}

	int SelectorItem::score() const {
		int score = 0;
		score += _pseudoClass.empty() ? 0 : 1;
		score += _tag.empty() ? 0 : 2;
		score += _styleClass.empty() ? 0 : 4;
		score += _name.empty() ? 0 : 8;
		score += !_self ? 0 : 16;
		return score;
	}

	bool SelectorItem::useTag() const {
		return !_tag.empty();
	}

	bool SelectorItem::useName() const {
		return !_name.empty();
	}

	bool SelectorItem::useClass() const {
		return !_styleClass.empty();
	}

	bool SelectorItem::usePseudo() const {
		return !_pseudoClass.empty();
	}

	bool operator==(const SelectorItem& lhs, const SelectorItem& rhs) {
		return lhs.to_string() == rhs.to_string();
	}

	bool operator!=(const SelectorItem& lhs, const SelectorItem& rhs) {
		return !(lhs == rhs);
	}
}

