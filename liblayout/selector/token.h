#ifndef LAYOUT_SELECTOR_TOKENIZER_H
#define LAYOUT_SELECTOR_TOKENIZER_H

#include "tokenizerbase.h"

namespace layout {
	namespace selector {
		enum class TokenType {
			word, number, symbol
		};

		enum class Symbol {
			none, child_sign, parent_sign,
			sibling_sign, name_sign, class_sign,
			pseudo_sign, pluss, minus, self
		};

		Symbol stringToSymbol(std::string);
		std::string symbolToString(Symbol);

		class WordToken : public tokenizer::Token {
		public:
			WordToken(std::string word);
			std::string word;
			virtual int type() { return (int)TokenType::word; }
			virtual std::string to_string() {
				return "{word:'"+word+"'}";
			};
		};

		class NumberToken : public tokenizer::Token {
			public:
			NumberToken(float number);
			int number;
			virtual int type() { return (int)TokenType::number; }
			virtual std::string to_string(){
				return "{number:"+std::to_string(number)+"}";
			};
		};

		class SymbolToken : public tokenizer::Token {
			public:
			SymbolToken(Symbol symbol);
			Symbol symbol;
			virtual int type() { return (int)TokenType::symbol; }
			virtual std::string to_string() {
				return "{symbol:"+symbolToString(symbol)+"}";
			};
		};

		class TokenSet : public tokenizer::TokenSet {
		public:
			TokenSet(std::string text);

			TokenType type() const;

			bool isWord() const;
			bool isWord(std::string condition) const;
			bool isNumber() const;
			bool isSymbol() const;
			bool isSymbol(Symbol condition) const;

			std::string getWord() const;
			int getNumber() const;
			Symbol getSymbol() const;
		};
	}
}

#endif
