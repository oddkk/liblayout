#include "layout/vec2.h"

namespace layout {
	Vec2& Vec2::operator+=(const Vec2& rhs) {
		x += rhs.x;
		y += rhs.y;
		return *this;
	}

	Vec2& Vec2::operator-=(const Vec2& rhs) {
		x -= rhs.x;
		y -= rhs.y;
		return *this;
	}

	Vec2& Vec2::operator*=(const Vec2& rhs) {
		x *= rhs.x;
		y *= rhs.y;
		return *this;
	}

	Vec2& Vec2::operator/=(const Vec2& rhs) {
		x /= rhs.x;
		y /= rhs.y;
		return *this;
	}


	Vec2 Vec2::operator+(const Vec2& rhs) const {
		Vec2 v = *this;
		v += rhs;
		return v;
	}

	Vec2 Vec2::operator-(const Vec2& rhs) const {
		Vec2 v = *this;
		v -= rhs;
		return v;
	}

	Vec2 Vec2::operator*(const Vec2& rhs) const {
		Vec2 v = *this;
		v *= rhs;
		return v;
	}

	Vec2 Vec2::operator/(const Vec2& rhs) const {
		Vec2 v = *this;
		v /= rhs;
		return v;
	}
}
