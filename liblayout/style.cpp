#include "layout/style.h"
#include "strutils.h"
#include "utf8.h"

#include <iostream>

namespace layout {
	StyleRule::StyleRule() {}

	StyleRule::StyleRule(std::string rule) {
		str::iterator iter = rule.begin();
		std::string sel = str::readUntil(iter, rule.end(), '{');
		if (iter != rule.end()) utf8::next(iter, rule.end());
		std::string declBlock = str::readUntil(iter, rule.end(), '}');
		if (iter != rule.end()) utf8::next(iter, rule.end());

		selector = Selector(str::trimWhitespace(sel));

		std::vector<std::string> decls = str::split(declBlock, ';');

		for (std::string decl : decls) {
			Declaration d = Declaration(str::trimWhitespace(decl), selector);
			if (!d.property().empty())
				declarations.push_back(d);
		}
	}

	StyleSheet::StyleSheet() {}

	StyleSheet::StyleSheet(std::string sheet) {
		str::iterator iter = sheet.begin();
		while (iter != sheet.end()) {
			std::string rule = str::readUntil(iter, sheet.end(), '}');
			if (iter != sheet.end()) utf8::next(iter, sheet.end());
			rules.push_back(StyleRule(rule));
		}
	}
}
