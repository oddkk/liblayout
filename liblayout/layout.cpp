#include "layout/layout.h"

#include "layout/element/root.h"

namespace layout {
	Layout::Layout()
		: _root(new element::Root(this)) {
	}

	void Layout::accept(Visitor* visitor) {
		_root->accept(visitor);
	}

	Element* Layout::root() const {
		return _root.get();
	}

	ElementList Layout::select(const Selector& sel) const {
		return _root->select(sel);
	}

	ElementList Layout::select(std::string str) const {
		return select(Selector(str));
	}

	void Layout::applyStyle(const StyleSheet& sheet) {
		for (const StyleRule& rule : sheet.rules) {
			applyStyle(rule);
		}
	}

	void Layout::applyStyle(const StyleRule& rule) {
		_rules.push_back(rule);
		select(rule.selector).applyStyle(rule);
	}

	Vec2 Layout::viewportSize() const {
		return _viewportSize;
	}

	void Layout::setViewportSize(Vec2 size) {
		_viewportSize = size;
	}

	float Layout::dpi() const {
		return _dpi;
	}

	void Layout::setDpi(float dpi) {
		_dpi = dpi;
	}
}

