#include "layout/element.h"
#include "element/defaults.h"
#include "layout/layout.h"

namespace layout {
	Element::Element()
		: name(),
		  styleClass(),
		  pseudoClass(),
		  _layout(nullptr),
		  _parent(nullptr) {
		rectangle.bind(this);
	}

	void Element::accept(Visitor* visitor) {
		visitor->push(this);
		visitor->visit(this);
		_acceptChildren(visitor);
		visitor->pop(this);
	}

	void Element::deleteChild(Element* element) {
		for (unsigned int i = 0; i < _children.size(); i++) {
			if (_children[i].get() == element) {
				_children.erase(_children.begin() + i);
				return;
			}
		}
	}

	void Element::deleteChild(const ElementList& elements) {
		for (Element* element : elements)
			deleteChild(element);
	}

	void Element::appendChild(Element* child) {
		child->_layout = _layout;
		child->_parent = this;
		_children.push_back(std::unique_ptr<Element>(child));
		child->clearDeclarations();
		child->checkGlobalDeclarations();
		child->raise("added");
	}

	void Element::appendChild(const ElementList& elements) {
		for (Element* element : elements)
			appendChild(element);
	}

	void Element::move(Element* newParent) {
		for (unsigned int i = 0; i < _parent->_children.size(); i++) {
			if (_parent->_children[i].get() == this) {
				newParent->_children.push_back(
					std::move(_parent->_children[i]));
				_parent->_children.erase(
					_parent->_children.begin() + i);
				break;
			}
		}
		_layout = newParent->_layout;
		_parent = newParent;
	}

	Element* Element::offset(int o) const {
		for (unsigned int i = 0; i < _parent->_children.size(); i++) {
			if (_parent->_children[i].get() == this) {
				int j = i + o;
				if (j < 0) j = 0;
				if (j >= _parent->_children.size())
					j = _parent->_children.size() - 1;
				return _parent->_children[j].get();
			}
		}
		return (Element*)this;
	}

	ElementList Element::children() const {
		ElementList elements;
		for (auto& child : _children)
			elements.append(child.get());
		return elements;
	}

	ElementList Element::decendents() const {
		ElementList elements;
		for (auto& child : _children) {
			elements.append(child.get());
			elements.append(child->decendents());
		}
		return elements;
	}

	ElementList Element::siblings() const {
		ElementList elements;
		for (auto& sibling : _parent->_children) {
			if (sibling.get() != this)
				elements.append(sibling.get());
		}
		return elements;
	}

	static ElementList findDecendents(Element const* e) {
		return e->decendents();
	}

	static ElementList findChildren(Element const* e) {
		return e->children();
	}

	static ElementList findSiblings(Element const* e) {
		return e->siblings();
	}

	struct {
		SelectorOperator op;
		ElementList(*function)(Element const*);
	} selectorOperatorHandler[] = {
		{ SelectorOperator::none,      findDecendents },
		{ SelectorOperator::decendent, findDecendents },
		{ SelectorOperator::child,     findChildren   },
		{ SelectorOperator::sibling,   findSiblings   },
	};

	const unsigned int selectorOperatorHandlerSize =
		sizeof(selectorOperatorHandler) /
		sizeof(selectorOperatorHandler[0]);

	ElementList Element::select(const Selector& sel) const {
		if (sel.items.empty())
			return ElementList({ const_cast<Element*>(this) });
		SelectorItem item = sel.items.front();
		ElementList foundElements;
		for (unsigned int i = 0;
		     i < selectorOperatorHandlerSize;i++) {
			if (selectorOperatorHandler[i].op == item.op()) {
				foundElements =
					selectorOperatorHandler[i].function(this);
				break;
			}
		}
		ElementList result;
		for (Element* e : foundElements) {
			if (e->matches(item))
				result.append(
					e->select(sel.subsection(1, sel.items.size())));
		}
		return result;
	}

	ElementList Element::select(std::string val) const {
		return select(Selector(val));
	}

	Layout* Element::layout() const {
		return _layout;
	}

	Element* Element::parent() const {
		return _parent;
	}

	bool Element::matches(const SelectorItem& itm) const {
		if (itm.useTag() && itm.tag() != tagName())
			return false;
		if (itm.useName() && itm.name() != name)
			return false;
		if (itm.useClass() && !itm.styleClass().hasActive(styleClass))
			return false;
		if (itm.usePseudo() && !itm.pseudoClass().hasActive(pseudoClass))
			return false;
		return true;
	}

	bool Element::matches(const Selector& sel) const {
		return _layout->select(sel).has(this);
	}

	void Element::applyStyle(const StyleRule& style) {
		for (const Declaration& decl : style.declarations)
			applyDeclaration(decl);
	}

	void Element::checkGlobalDeclarations() {
		if (!layout())
			return;
		for (const auto& iter : layout()->_rules)
			applyStyle(iter);
	}

	Context Element::getContext() {
		Context ctx = layout()->funcContext;
		ctx.set("element", this);
		ctx.set("layout", this->layout());
		return ctx;
	}

	expression::Value Element::operator[](std::string property) {
		return getDeclaration(property).get(getContext());
	}

	void Element::_acceptChildren(Visitor* v) {
		for (unsigned int i = 0; i < _children.size(); i++)
			_children[i]->accept(v);
	}

	bool Element::_matchesSelector(Selector sel) const {
		return matches(sel);
	}

	IEventReceiver* Element::_getParentEventReceiver() const {
		return (IEventReceiver*)parent();
	}

	std::string Element::_visitableName() const {
		return tagName();
	}

	static std::map<std::string,
	                Element*(*)()> _elementConstructors;

	Element* Element::construct(std::string tagName) {
		initializeDefaultElements();
		auto iter = _elementConstructors.find(tagName);
		if (iter != _elementConstructors.end())
			return iter->second();
		return nullptr;
	}

	void Element::registerElement(Element*(*constructor)()) {
		Element* e = constructor();
		_elementConstructors[e->tagName()] = constructor;
		delete e;
	}
}
