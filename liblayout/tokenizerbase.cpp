#include "tokenizerbase.h"
#include "utf8.h"

namespace layout {
	namespace tokenizer {
		TokenSet::TokenSet(std::string text,
					TokenizerMethod* methods,
					unsigned int methodsCount)
				: index(0) {
			std::string::iterator it = text.begin();
			while (it != text.end()) {
				unsigned int c = utf8::peek_next(it, text.end());
				bool tokenized = false;
				for (unsigned int i = 0; i < methodsCount; i++) {
					if (methods[i].condition(c)) {
						methods[i].tokenize(c, it, text.end(), tokens);
						tokenized = true;
						break;
					}
				}
				if (!tokenized)
					utf8::next(it, text.end());
			}
		}

		bool TokenSet::eos() const {
			return index >= tokens.size();
		}

		void TokenSet::reset() {
			index = 0;
		}

		void TokenSet::next() {
			index++;
		}

		Token* TokenSet::current() const {
			return tokens[index].get();
		}

		Token* TokenSet::peek_previous() const {
			if ((int)index - 1 < 0) return nullptr;
			return tokens[index - 1].get();
		}

		Token* TokenSet::peek_next() const {
			if (index + 1 > tokens.size()) return nullptr;
			return tokens[index + 1].get();
		}

		int TokenSet::type_int() const {
			return current()->type();
		}

		std::string TokenSet::to_string() const {
			std::string res = "";
			for (unsigned int i = 0; i < tokens.size(); i++)
				res += tokens[i]->to_string();
			return res;
		}

		InvalidTokenException::InvalidTokenException(Token* t) {
			_what = "Invalid use of token " + t->to_string();
		}

		const char* InvalidTokenException::what() const throw() {
			return _what.c_str();
		}
	}
}

