#include "layout/context.h"
#include "layout/layout.h"

REGISTER_CONTEXT_ENTRY_TYPE(std::string);
REGISTER_CONTEXT_ENTRY_TYPE(int);
REGISTER_CONTEXT_ENTRY_TYPE(float);
REGISTER_CONTEXT_ENTRY_TYPE(double);
REGISTER_CONTEXT_ENTRY_TYPE(layout::Layout*);
REGISTER_CONTEXT_ENTRY_TYPE(layout::Element*);

namespace layout {
	_ContextEntry::_ContextEntry(std::string name)
		: name(name) {}

	Context::Context() {}

	Context::Context(const Context& ctx) {
		for (auto& entry : ctx._entries)
			_entries[entry.first] =
				std::unique_ptr<_ContextEntry>(entry.second->clone());
	}

	void Context::_set(_ContextEntry* entry) {
		_entries[entry->name] = std::unique_ptr<_ContextEntry>(entry);
	}

	_ContextEntry* Context::_get(std::string name) const {
		auto iter = _entries.find(name);
		if (iter == _entries.end())
			throw ContextVariableNotSetException(name);
		return iter->second.get();
	}

	ContextWrongTypeException::ContextWrongTypeException(std::string name,
														 std::string dt,
														 std::string et)
		: name(name), dataType(dt), expectedType(et) {
			_what = "Attempted to get context variable '" + name +
				"' as a '" + expectedType + "', but it is a '" +
				dataType + "'";
	}

	const char* ContextWrongTypeException::what() const throw() {
		return _what.c_str();
	}

	ContextVariableNotSetException::
		ContextVariableNotSetException(std::string name)
		: name(name) {
			_what = "Attempted to get context variable '" + name +
				"', but it does not exist";
	}

	const char* ContextVariableNotSetException::what() const throw() {
		return _what.c_str();
	}
}

