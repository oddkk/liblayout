#include "layout/property.h"
#include "layout/element.h"
#include "layout/layout.h"

namespace layout {
	Property::Property()
		: _owner(nullptr),
		  _name(),
		  _values(nullptr),
		  _setValue(),
		  _setValueSet(false)
	{}

	void Property::bind(Element* element, std::string name) {
		_owner = element;
		_name = name;
		_values = element->_getDeclList(name);
	}

	std::string Property::name() const {
		return _name;
	}

	expression::Value Property::get() const {
		if (_setValueSet)
			return _setValue;
		if (_values)
			return _values->get().get(_getContext());
		return expression::Value();
	}

	Context Property::_getContext() const {
		return _owner->getContext();
	}

	void Property::resetSet() {
		_setValue = expression::Value();
		_setValueSet = false;
	}

	void Property::set(const expression::Value& val) {
		_setValue = val;
		_setValueSet = true;
	}

	Property& Property::operator=(const expression::Value& val) {
		set(val);
		return *this;
	}

	Property& Property::operator=(std::string val) {
		set(expression::Value(val));
		return *this;
	}

	Property& Property::operator=(float val) {
		set(expression::Value(val));
		return *this;
	}

	Property& Property::operator=(bool val) {
		set(expression::Value(val));
		return *this;
	}

	Property& Property::operator=(int val) {
		set(expression::Value((float)val));
		return *this;
	}

	Property::operator expression::Value() const {
		return get();
	}

	Property::operator std::string() const {
		return get().text();
	}

	Property::operator float() const {
		return get().number();
	}

	Property::operator bool() const {
		return get().boolean();
	}

	Property::operator int() const {
		return (int)get().number();
	}

	bool Property::operator==(const std::string& rhs) const {
		return get().text() == rhs;
	}

	bool Property::operator==(float rhs) const {
		return get().number() == rhs;
	}

	bool Property::operator==(bool rhs) const {
		return get().boolean() == rhs;
	}

	bool Property::operator==(int rhs) const {
		return (int)get().number() == rhs;
	}

}
