#include "layout/elementlist.h"
#include "layout/element.h"

namespace layout {
	ElementList::ElementList()
		: _elements() {}

	ElementList::ElementList(const std::vector<Element*>& elements)
		: _elements(elements) {}

	ElementList::ElementList(std::initializer_list<Element*> list)
		: _elements(list.begin(), list.end()) {}

	void ElementList::append(Element* element) {
		_elements.push_back(element);
	}

	void ElementList::append(const ElementList& list) {
		_elements.insert(_elements.end(), list.begin(), list.end());
	}

	void ElementList::remove(Element const* element) {
		removeAt(elementIndex(element));
	}

	void ElementList::removeAt(unsigned int index) {
		if (index >= _elements.size())
			return;
		_elements.erase(_elements.begin() + index); 
	}

	void ElementList::remove(const ElementList& list) {
		for (Element* element : list)
			remove(element);
	}

	void ElementList::clear() {
		_elements.clear();
	}

	unsigned int ElementList::elementIndex(Element const* element) const {
		for (unsigned int i = 0; i < _elements.size(); i++) {
			if (_elements[i] == element)
				return i;
		}
		return -1;
	}

	bool ElementList::empty() const {
		return _elements.empty();
	}

	unsigned int ElementList::size() const {
		return _elements.size();
	}

	Element* ElementList::operator[](unsigned int i) const {
		return _elements[i];
	}

	ElementIterator ElementList::begin() {
		return _elements.begin();
	}

	ConstElementIterator ElementList::begin() const {
		return _elements.begin();
	}

	ElementIterator ElementList::end() {
		return _elements.end();
	}

	ConstElementIterator ElementList::end() const {
		return _elements.end();
	}

	bool ElementList::has(Element const* element) const {
		return elementIndex(element) != -1;
	}

	bool ElementList::has(const ElementList& list) const {
		for (Element* element : list) {
			if (!has(element))
				return false;
		}
		return true;
	}

	void ElementList::forEach(void(*func)(Element*, unsigned int)) {
		for (unsigned int i = 0; i < _elements.size(); i++)
			func(_elements[i], i);
	}

	void ElementList::applyStyle(const StyleRule& rule) {
		for (Element* element : _elements)
			element->applyStyle(rule);
	}

	void ElementList::applyDeclaration(const Declaration& decl) {
		for (Element* element : _elements)
			element->applyDeclaration(decl);
	}

	void ElementList::on(std::string eventName, EventCallbackFunction callback) {
		for (Element* element : _elements)
			element->on(eventName, callback);
	}

	void ElementList::on(EventCallback callback) {
		for (Element* element : _elements)
			element->on(callback);
	}
}
