#include "../catch.hpp"
#include "layout/layout.h"
#include "layout/element.h"
#include "layout/element/root.h"
#include "layout/element/button.h"
#include "layout/element/container.h"

using namespace layout;
using namespace layout::element;

TEST_CASE("select", "[element][selector]") {
	Layout l;
	Root root (&l);
	Container* c1 = new Container();
	Container* c2 = new Container();
	Button* b1 = new Button();
	Button* b2 = new Button();
	Button* b3 = new Button();

	root.appendChild({ c1, c2 });
	c1->appendChild(b1);
	c2->appendChild(b2);

	b2->appendChild(b3);

	c1->name = "c1";
	c2->name = "c2";

	b1->name = "b1";
	b2->name = "b2";

	c2->styleClass.activate("class");

	SECTION("tag") {
		ElementList selected = root.select("button");
		REQUIRE(selected.has({ b1, b2, b3 }));
		REQUIRE(selected.size() == 3);
	}

	SECTION("name") {
		ElementList selected = root.select("#b1");
		REQUIRE(selected.has({ b1 }));
		REQUIRE(selected.size() == 1);
	}

	SECTION("class") {
		ElementList selected = root.select(".class");
		REQUIRE(selected.has({ c2 }));
		REQUIRE(selected.size() == 1);
	}

	SECTION("child") {
		ElementList selected = root.select(".class > button");
		REQUIRE(selected.has({ b2 }));
		REQUIRE(selected.size() == 1);
	}

	SECTION("decendent") {
		ElementList selected = root.select(".class button");
		REQUIRE(selected.has({ b2, b3 }));
		REQUIRE(selected.size() == 2);
	}

	SECTION("sibling") {
		ElementList selected = root.select(".class ~ #c1");
		REQUIRE(selected.has({ c1 }));
		REQUIRE(selected.size() == 1);
	}

	SECTION("pseudo") {
		ElementList selected = root.select(":pseudo");
		REQUIRE(selected.size() == 0);

		b1->pseudoClass.activate("pseudo");
		selected = root.select(":pseudo");
		REQUIRE(selected.has({ b1 }));
		REQUIRE(selected.size() == 1);

		b1->pseudoClass.deactivate("pseudo");
		selected = root.select(":pseudo");
		REQUIRE(selected.size() == 0);
	}
}
