#include "../catch.hpp"
#include "layout/layout.h"
#include "layout/element/button.h"

using namespace layout;

TEST_CASE("load layout", "[layout][load]") {
	Layout l;

	std::string file =
"<layout>\
	<style>\
	.class #name2 {\
		text: \"foo\";\
		font-family: \"bar\";\
	}\
	</style>\
	<container>\
		<button name='name1'/>\
	</container>\
	<container class='class'>\
		<button name='name2'/>\
	</container>\
</layout>";

	l.load(file);

	ElementList children = l.root()->children();

	REQUIRE(children.size() == 3);
	REQUIRE(children[1]->tagName() == "container");

	REQUIRE(children[1]->children().size() == 1);
	REQUIRE(children[1]->children()[0]->tagName() == "button");
	REQUIRE(children[1]->children()[0]->name == "name1");

	REQUIRE(((element::Button*)children[1]->children()[0])->text.get().text() == "");
	REQUIRE(((element::Button*)children[1]->children()[0])->font.family.get().text() == "");

	REQUIRE(children[2]->tagName() == "container");
	REQUIRE(children[2]->styleClass.isActive("class"));

	REQUIRE(children[2]->children().size() == 1);
	REQUIRE(children[2]->children()[0]->tagName() == "button");
	REQUIRE(children[2]->children()[0]->name == "name2");

	REQUIRE(((element::Button*)children[2]->children()[0])->text.get().text() == "foo");
	REQUIRE(((element::Button*)children[2]->children()[0])->font.family.get().text() == "bar");

}
