#include "catch.hpp"
#include "layout/element.h"
#include "layout/element/container.h"

using namespace layout;
using namespace layout::element;

TEST_CASE("element list", "[elementlist]") {
	ElementList list;
	ElementList list2;
	Container c1;
	Container c2;
	Container c3;
	Container c4;
	Container c5;
	Container c6;

	REQUIRE(list.empty());
	REQUIRE(list.size() == 0);
	
	list.append(&c1);
	list.append({ &c2, &c3 });
	REQUIRE(list.has(&c1));
	REQUIRE(list.has(&c2));
	REQUIRE(list.has(&c3));

	REQUIRE(!list.empty());
	REQUIRE(list.size() == 3);

	list2.append(&c4);
	list2.append(&c5);
	list2.append(&c6);

	list.append(list2);

	REQUIRE(list.has(&c4));
	REQUIRE(list.has(&c5));
	REQUIRE(list.has(&c6));
	REQUIRE(list.has(list2));
	
	REQUIRE(list.size() == 6);

	REQUIRE(list.elementIndex(&c2) == 1);
	REQUIRE(list.elementIndex(&c6) == 5);

	REQUIRE(list[5] == &c6);

	REQUIRE(list.has({ &c1, &c2, &c3, &c4, &c5, &c6 }));

	list.removeAt(0);
	REQUIRE(list.size() == 5);
	REQUIRE(!list.has(&c1));

	list.remove(&c2);
	REQUIRE(list.size() == 4);
	REQUIRE(!list.has(&c2));

	list.remove(list2);
	REQUIRE(!list.has(list2));
	REQUIRE(list.size() == 1);
	REQUIRE(list.has(&c3));
	
	list.clear();
	REQUIRE(list.empty());
	REQUIRE(list.size() == 0);
	REQUIRE(!list.has(&c3));
}
