#include "../catch.hpp"
#include "layout/expression/expression.h"
#include "layout/expression/functions.h"
#include <iostream>

using namespace layout;
using namespace layout::expression;

TEST_CASE("simple number addition", "[expression][parsing]") {
	Expression expr ("2+3");
	Value val = expr.eval();
	REQUIRE(val.type() == ValueType::number);
	REQUIRE(val.number() == 5);
}

TEST_CASE("simple string concatination", "[expression][parsing]") {
	Expression expr ("\"This is \"+\"a test\"");
	Value val = expr.eval();
	REQUIRE(val.type() == ValueType::text);
	REQUIRE(val.text() == "This is a test");
}

TEST_CASE("simple boolean true", "[expression][parsing]") {
	Expression expr ("true");
	Value val = expr.eval();
	REQUIRE(val.type() == ValueType::boolean);
	REQUIRE(val.boolean() == true);
}

TEST_CASE("simple boolean false", "[expression][parsing]") {
	Expression expr ("false");
	Value val = expr.eval();
	REQUIRE(val.type() == ValueType::boolean);
	REQUIRE(val.boolean() == false);
}

TEST_CASE("simple none", "[expression][parsing]") {
	Expression expr ("none");
	Value val = expr.eval();
	REQUIRE(val.type() == ValueType::none);
	REQUIRE(val.none());
}

TEST_CASE("parenthesis", "[expression][parsing]") {
	Expression expr ("(3+5)");
	Value val = expr.eval();
	REQUIRE(val.type() == ValueType::number);
	REQUIRE(val.number() == 8);
}

TEST_CASE("order of operations", "[expression][parsing]") {
	Expression expr ("2*(3+5)");
	Value val = expr.eval();
	REQUIRE(val.type() == ValueType::number);
	REQUIRE(val.number() == 16);
}

TEST_CASE("function", "[expression][parsing]") {
	Expression expr ("cos(0)");
	Value val = expr.eval();
	REQUIRE(val.type() == ValueType::number);
	REQUIRE(val.number() == 1);
}

TEST_CASE("postfix", "[expression][parsing]") {
	Expression expr ("0cos");
	Value val = expr.eval();
	REQUIRE(val.type() == ValueType::number);
	REQUIRE(val.number() == 1);

	registerFunction("two",
		 FunctionDefinition(2, [](const FunctionArgs& fa) {
							return fa.args[0] + fa.args[1];
						}));
	REQUIRE_THROWS(Expression ("2two"));
	REQUIRE_THROWS(Expression ("2pi"));
}

TEST_CASE("wrong number of arguments", "[expression][parsing]") {
	registerFunction("foo",
		 FunctionDefinition(2, [](const FunctionArgs& fa) {
			return fa.args[0] + fa.args[1];
		}));
	Expression expr("foo()");
	REQUIRE_THROWS(expr.eval());
}

TEST_CASE("expr with context", "[expression][parsing]") {
	registerFunction("getctx",
		 FunctionDefinition(0, [](const FunctionArgs& fa) {
				return Value(fa.context.get<float>("foo"));
			}));

	Context ctx;
	Expression expr ("getctx()");
	ctx.set<float>("foo", 25.0f);
	Value val = expr.eval(ctx);
	REQUIRE(val.type() == ValueType::number);
	REQUIRE(val.number() == 25);
}

