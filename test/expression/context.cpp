#include "../catch.hpp"
#include "layout/context.h"

using namespace layout;

TEST_CASE("context", "[expression][context]") {
	Context ctx;

	ctx.set<int>("foo", 25);

	REQUIRE(ctx.get<int>("foo") == 25);
	REQUIRE_THROWS(ctx.get<float>("foo"));
}
