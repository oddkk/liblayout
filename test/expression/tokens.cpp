#include "../catch.hpp"
#include "expression/token.h"

using namespace layout::expression;

TEST_CASE("parse word", "[expression][token]") {
	TokenSet ts ("word");
	REQUIRE(ts.type() == TokenType::word);
	REQUIRE(ts.getWord() == "word");
	ts.next();
	REQUIRE(ts.eos());
}

TEST_CASE("parse text", "[expression][token]") {
	std::string testString = "";
	SECTION("' style") {
		testString = "'this is text'";
	}
	SECTION("\" style") {
		testString = "\"this is text\"";
	}
	TokenSet ts (testString);
	REQUIRE(ts.type() == TokenType::text);
	REQUIRE(ts.getText() == "this is text");
	ts.next();
	REQUIRE(ts.eos());
}

TEST_CASE("parse number", "[expression][token]") {
	std::string testString = "";
	float expected;
	SECTION("integer") {
		testString = "314";
		expected = 314;
	}
	SECTION("decimal") {
		testString = "3.14";
		expected = 3.14f;
	}
	TokenSet ts (testString);
	REQUIRE(ts.type() == TokenType::number);
	REQUIRE(ts.getNumber() == expected);
	ts.next();
	REQUIRE(ts.eos());
}

TEST_CASE("parse symbols", "[expression][token]") {
	struct {
		std::string test;
		Symbol expected;
	} cases[] = {
		{ "+", Symbol::pluss },
		{ "-", Symbol::minus },
		{ "*", Symbol::multiply },
		{ "/", Symbol::divide },
		{ "%", Symbol::modulo },
		{ "(", Symbol::parenthesis_open },
		{ ")", Symbol::parenthesis_close },
		{ ",", Symbol::comma },
	};
	for (int i = 0; i < sizeof(cases) / sizeof(cases[0]); i++) {
		TokenSet ts (cases[i].test);
		REQUIRE(ts.type() == TokenType::symbol);
		REQUIRE(ts.getSymbol() == cases[i].expected);
		ts.next();
		REQUIRE(ts.eos());
	}
}

struct Expected {
	TokenType type;
	std::string text;
	std::string word;
	float number;
	Symbol symbol;

	static Expected Text(std::string text) {
		Expected ex;
		ex.type = TokenType::text;
		ex.text = text;
		return ex;
	}
	static Expected Word(std::string text) {
		Expected ex;
		ex.type = TokenType::word;
		ex.word = text;
		return ex;
	}
	static Expected Number(float number) {
		Expected ex;
		ex.type = TokenType::number;
		ex.number = number;
		return ex;
	}
	static Expected Sym(Symbol symbol) {
		Expected ex;
		ex.type = TokenType::symbol;
		ex.symbol = symbol;
		return ex;
	}
};

static void doTest(std::string testString, Expected expected[], int numExpected) {
	TokenSet ts (testString);
	for (int i = 0; i < numExpected; i++) {
		REQUIRE(ts.type() == expected[i].type);
		switch (expected[i].type) {
		case TokenType::word:   REQUIRE(ts.getWord()   == expected[i].word);   break;
		case TokenType::text:   REQUIRE(ts.getText()   == expected[i].text);   break;
		case TokenType::number: REQUIRE(ts.getNumber() == expected[i].number); break;
		case TokenType::symbol: REQUIRE(ts.getSymbol() == expected[i].symbol); break;
		default: REQUIRE(false);
		}
		ts.next();
	}
	REQUIRE(ts.eos());
}

TEST_CASE("parse complex", "[expression][token]") {
	std::string testString = "2x+3*\"Text string\" word ";
	Expected exp[] = {
		Expected::Number(2),
		Expected::Word("x"),
		Expected::Sym(Symbol::pluss),
		Expected::Number(3),
		Expected::Sym(Symbol::multiply),
		Expected::Text("Text string"),
		Expected::Word("word"),
	};
	doTest(testString, exp, sizeof(exp) / sizeof(Expected));
}

TEST_CASE("parse parenthesis", "[expression][token]") {
	std::string testString = "2*(3+5)";
	Expected exp[] = {
		Expected::Number(2),
		Expected::Sym(Symbol::multiply),
		Expected::Sym(Symbol::parenthesis_open),
		Expected::Number(3),
		Expected::Sym(Symbol::pluss),
		Expected::Number(5),
		Expected::Sym(Symbol::parenthesis_close),
	};
	doTest(testString, exp, sizeof(exp) / sizeof(Expected));
}

