#include "../catch.hpp"
#include "layout/layout.h"
#include "layout/element/button.h"

using namespace layout;
using namespace layout::element;

TEST_CASE("function propertyof", "[function][expression]") {
	Layout l;

	l.load(
"<layout>"
"<style>"
"#btn1 { text:\"foo\"; }"
"#btn2 { text:propertyof(\"#btn1\",\"text\"); }"
"#btn3 { text:propertyof(\"#btn3\",\"text\"); }"
"</style>"
"<button name='btn1'/>"
"<button name='btn2'/>"
"<button name='btn3'/>"
"</layout>");

	Button* btn1 = (Button*)l.select("#btn1")[0];
	Button* btn2 = (Button*)l.select("#btn2")[0];
	Button* btn3 = (Button*)l.select("#btn3")[0];

	REQUIRE((std::string)btn1->text == "foo");
	REQUIRE((std::string)btn2->text == "foo");
	REQUIRE((std::string)btn3->text == "");
}

