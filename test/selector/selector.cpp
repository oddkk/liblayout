#include "../catch.hpp"
#include "layout/selector.h"

using namespace layout;

static void testItem(SelectorItem item,
		std::string tag, std::string name,
		std::vector<std::string> classes,
		std::vector<std::string> pseudo,
		int offset, bool self,
		SelectorOperator op) {
	REQUIRE(item.tag() == tag);
	REQUIRE(item.name() == name);

	ClassList classList;
	for (unsigned int i = 0; i < classes.size(); i++)
		classList.activate(classes[i]);
	REQUIRE(item.styleClass().hasAllActive(classList));

	ClassList pseudoList;
	for (unsigned int i = 0; i < pseudo.size(); i++)
		pseudoList.activate(pseudo[i]);
	REQUIRE(item.pseudoClass().hasAllActive(pseudoList));

	REQUIRE(item.offset() == offset);
	REQUIRE(item.self() == self);
	REQUIRE(item.op() == op);
}

TEST_CASE("selector parse single", "[selector][parse]") {
	Selector sel ("tag#name.class:pseudo+1");
	REQUIRE(sel.items.size() == 1);
	testItem(sel.items[0], "tag", "name",
			std::vector<std::string>({ "class" }),
			std::vector<std::string>({ "pseudo" }),
			1, false, SelectorOperator::decendent);
}

TEST_CASE("selector parse multiple", "[selector][parse]") {
	Selector sel ("tag#name.class:pseudo+1 > tag1#name1.class1:pseudo1+2");
	REQUIRE(sel.items.size() == 2);
	testItem(sel.items[0], "tag", "name",
			std::vector<std::string>({ "class" }),
			std::vector<std::string>({ "pseudo" }),
			1, false, SelectorOperator::decendent);
	testItem(sel.items[1], "tag1", "name1",
			std::vector<std::string>({ "class1" }),
			std::vector<std::string>({ "pseudo1" }),
			2, false, SelectorOperator::child);
}

TEST_CASE("selector subsection", "[selector]") {
	Selector sel ("tag#name tag.class tag:pseudo");
	REQUIRE(sel.items.size() == 3);
	Selector sel2 = sel.subsection(1, 3);
	REQUIRE(sel2.items.size() == 2);
	REQUIRE(sel2.items[0] == sel.items[1]);
	REQUIRE(sel2.items[1] == sel.items[2]);
}

