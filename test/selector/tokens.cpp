#include "../catch.hpp"
#include "selector/token.h"

#include <iostream>

using namespace layout::selector;

TEST_CASE("selector parse word", "[selector][token]") {
	TokenSet ts ("word");
	REQUIRE(ts.type() == TokenType::word);
	REQUIRE(ts.getWord() == "word");
	ts.next();
	REQUIRE(ts.eos());
}

TEST_CASE("selector parse number", "[selector][token]") {
	TokenSet ts ("314");
	REQUIRE(ts.type() == TokenType::number);
	REQUIRE(ts.getNumber() == 314);
	ts.next();
	REQUIRE(ts.eos());
}

TEST_CASE("selector parse symbols", "[selector][token]") {
	struct {
		std::string test;
		Symbol expected;
	} cases[] = {
		{ ">", Symbol::child_sign },
		{ "<", Symbol::parent_sign },
		{ "~", Symbol::sibling_sign},
		{ "#", Symbol::name_sign },
		{ ".", Symbol::class_sign },
		{ ":", Symbol::pseudo_sign },
		{ "+", Symbol::pluss },
		{ "-", Symbol::minus },
	};
	for (int i = 0; i < sizeof(cases) / sizeof(cases[0]); i++) {
		TokenSet ts (cases[i].test);
		REQUIRE(ts.type() == TokenType::symbol);
		REQUIRE(ts.getSymbol() == cases[i].expected);
		ts.next();
		REQUIRE(ts.eos());
	}
}

struct Expected {
	TokenType type;
	std::string text;
	std::string word;
	int number;
	Symbol symbol;

	static Expected Word(std::string text) {
		Expected ex;
		ex.type = TokenType::word;
		ex.word = text;
		return ex;
	}
	static Expected Number(int number) {
		Expected ex;
		ex.type = TokenType::number;
		ex.number = number;
		return ex;
	}
	static Expected Sym(Symbol symbol) {
		Expected ex;
		ex.type = TokenType::symbol;
		ex.symbol = symbol;
		return ex;
	}

	std::string toString() {
		switch (type) {
		case TokenType::word: return "{word: " + this->word + "}";
		case TokenType::number: return "{number: " + std::to_string(this->number) + "}";
		case TokenType::symbol: return "{symbol: " + std::to_string((int)this->symbol) + "}";
		}
		return "none expected";
	}
};

std::string tokenTypeToString(TokenType tt) {
	switch (tt) {
	case TokenType::word: return "word";
	case TokenType::number: return "number";
	case TokenType::symbol: return "symbol";
	}
	return "none expected";
}

static void doTest(std::string testString, Expected expected[], int numExpected) {
	TokenSet ts (testString);
	for (int i = 0; i < numExpected; i++) {
		REQUIRE(ts.type() == expected[i].type);
		switch (expected[i].type) {
		case TokenType::word:   REQUIRE(ts.getWord()   == expected[i].word);   break;
		case TokenType::number: REQUIRE(ts.getNumber() == expected[i].number); break;
		case TokenType::symbol: REQUIRE(ts.getSymbol() == expected[i].symbol); break;
		default: REQUIRE(false);
		}
		ts.next();
	}
	REQUIRE(ts.eos());
}

TEST_CASE("selector parse complex", "[selector][token]") {
	std::string testString = "";
	SECTION("with space") {
		testString = "> tag#name.class1.class2:pseudo1:pseudo2+1";
	}
	SECTION("without space") {
		testString = ">tag#name.class1.class2:pseudo1:pseudo2+1";
	}
	Expected exp[] = {
		Expected::Sym(Symbol::child_sign),
		Expected::Word("tag"),
		Expected::Sym(Symbol::name_sign),
		Expected::Word("name"),
		Expected::Sym(Symbol::class_sign),
		Expected::Word("class1"),
		Expected::Sym(Symbol::class_sign),
		Expected::Word("class2"),
		Expected::Sym(Symbol::pseudo_sign),
		Expected::Word("pseudo1"),
		Expected::Sym(Symbol::pseudo_sign),
		Expected::Word("pseudo2"),
		Expected::Sym(Symbol::pluss),
		Expected::Number(1),
	};
	doTest(testString, exp, sizeof(exp) / sizeof(Expected));
}

