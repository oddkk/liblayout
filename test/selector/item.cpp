#include "../catch.hpp"
#include "layout/selector.h"

using namespace layout;

static void testItem(SelectorItem item,
		std::string tag, std::string name,
		std::vector<std::string> classes,
		std::vector<std::string> pseudo,
		int offset, bool self,
		SelectorOperator op) {
	REQUIRE(item.tag() == tag);
	REQUIRE(item.name() == name);

	ClassList classList;
	for (unsigned int i = 0; i < classes.size(); i++)
		classList.activate(classes[i]);
	REQUIRE(item.styleClass().hasAllActive(classList));

	ClassList pseudoList;
	for (unsigned int i = 0; i < pseudo.size(); i++)
		pseudoList.activate(pseudo[i]);
	REQUIRE(item.pseudoClass().hasAllActive(pseudoList));

	REQUIRE(item.offset() == offset);
	REQUIRE(item.self() == self);
	REQUIRE(item.op() == op);
}

TEST_CASE("selector item parse only tag", "[selector][selector item][parse]") {
	SelectorItem item ("tag");
	testItem(item, "tag", "",
			std::vector<std::string>(),
			std::vector<std::string>(),
			0, false, SelectorOperator::decendent);
}

TEST_CASE("selector item parse only name", "[selector][selector item][parse]") {
	SelectorItem item ("#name");
	testItem(item, "", "name",
			std::vector<std::string>(),
			std::vector<std::string>(),
			0, false, SelectorOperator::decendent);

}

TEST_CASE("selector item parse only class", "[selector][selector item][parse]") {
	SelectorItem item (".class");
	testItem(item, "", "",
			std::vector<std::string>({ "class" }),
			std::vector<std::string>(),
			0, false, SelectorOperator::decendent);
}

TEST_CASE("selector item parse multiple classes", "[selector][selector item][parse]") {
	SelectorItem item (".class1.class2.class3");
	testItem(item, "", "",
			std::vector<std::string>({
				"class1", "class2", "class3"
				}),
			std::vector<std::string>(),
			0, false, SelectorOperator::decendent);
}

TEST_CASE("selector item parse only pseudo", "[selector][selector item][parse]") {
	SelectorItem item (":pseudo");
	testItem(item, "", "",
			std::vector<std::string>(),
			std::vector<std::string>({ "pseudo" }),
			0, false, SelectorOperator::decendent);
}

TEST_CASE("selector item parse multiple pseudo", "[selector][selector item][parse]") {
	SelectorItem item (":pseudo1:pseudo2:pseudo3");
	testItem(item, "", "",
			std::vector<std::string>(),
			std::vector<std::string>({
				"pseudo1", "pseudo2", "pseudo3"
				}),
			0, false, SelectorOperator::decendent);
}

TEST_CASE("selector item parse only offset", "[selector][selector item][parse]") {
	SelectorItem item1 ("+1");
	testItem(item1, "", "",
			std::vector<std::string>(),
			std::vector<std::string>(),
			1, false, SelectorOperator::decendent);

	SelectorItem item2 ("-1");
	testItem(item2, "", "",
			std::vector<std::string>(),
			std::vector<std::string>(),
			-1, false, SelectorOperator::decendent);
}

TEST_CASE("selector item parse only self", "[selector][selector item][parse]") {
	SelectorItem item ("&");
	testItem(item, "", "",
			std::vector<std::string>(),
			std::vector<std::string>(),
			0, true, SelectorOperator::decendent);
}

TEST_CASE("selector item parse complex", "[selector][selector item][parse]") {
	SelectorItem item ("tag#name.class1.class2:pseudo1:pseudo2+1");
	testItem(item, "tag", "name",
			std::vector<std::string>({ "class1", "class2" }),
			std::vector<std::string>({ "pseudo1", "pseudo2" }),
			1, false, SelectorOperator::decendent);
}

TEST_CASE("selector item parse selector", "[selector][selector item][parse]") {
	SelectorItem item_dec (" tag");
	testItem(item_dec, "tag", "",
			std::vector<std::string>(),
			std::vector<std::string>(),
			0, false, SelectorOperator::decendent);

	SelectorItem item_child ("> tag");
	testItem(item_child, "tag", "",
			std::vector<std::string>(),
			std::vector<std::string>(),
			0, false, SelectorOperator::child);

	SelectorItem item_sibling ("~ tag");
	testItem(item_sibling, "tag", "",
			std::vector<std::string>(),
			std::vector<std::string>(),
			0, false, SelectorOperator::sibling);
}

