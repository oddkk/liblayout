#include "../catch.hpp"
#include "layout/selector.h"

using namespace layout;

TEST_CASE("selector scors", "[selector]") {
	Selector tag_sel("tag");
	Selector class_sel(".class");
	Selector name_sel("#name");
	Selector pseudo_sel(":pseudo");
	Selector self_sel("&");

	Selector tag_pseudo_sel("tag:pseudo");
	Selector class_pseudo_sel(".class:pseudo");
	Selector name_pseudo_sel("#name:pseudo");
	Selector self_pseudo_sel("&:pseudo");

	REQUIRE(tag_sel.score() < class_sel.score());
	REQUIRE(class_sel.score() < name_sel.score());
	REQUIRE(name_sel.score() < self_sel.score());

	REQUIRE(pseudo_sel.score() < tag_sel.score());
	REQUIRE(pseudo_sel.score() < class_sel.score());
	REQUIRE(pseudo_sel.score() < name_sel.score());

	REQUIRE(tag_pseudo_sel.score() > tag_sel.score());
	REQUIRE(class_pseudo_sel.score() > class_sel.score());
	REQUIRE(name_pseudo_sel.score() > name_sel.score());
	REQUIRE(self_pseudo_sel.score() > self_sel.score());

}
