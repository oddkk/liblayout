#include "catch.hpp"
#include "layout/style.h"

using namespace layout;

TEST_CASE("style rule", "[style][rule]") {
	StyleRule sr("tag#name { property1: 2+3; property2:3; }");

	REQUIRE(sr.selector.items.size() == 1);
	REQUIRE(sr.selector.items[0].tag() == "tag");
	REQUIRE(sr.selector.items[0].name() == "name");
	REQUIRE(sr.declarations.size() == 2);
	REQUIRE(sr.declarations[0].property() == "property1");
	REQUIRE(sr.declarations[0].get().number() == 5);

	REQUIRE(sr.declarations[1].property() == "property2");
	REQUIRE(sr.declarations[1].get().number() == 3);
}

TEST_CASE("style sheet", "[style][sheet]") {
	StyleSheet sheet("\
tag#name\
{\
	property1: 2+3;\
	property2:3;\
}\
tag2#name2 {\
	property3: \"Hello\";\
	property4: 25;\
}");
	REQUIRE(sheet.rules.size() == 2);
	REQUIRE(sheet.rules[0].selector.items.size() == 1);
	REQUIRE(sheet.rules[0].selector.items[0].tag() == "tag");
	REQUIRE(sheet.rules[0].selector.items[0].name() == "name");
	REQUIRE(sheet.rules[0].declarations.size() == 2);
	REQUIRE(sheet.rules[0].declarations[0].property() == "property1");
	REQUIRE(sheet.rules[0].declarations[0].get().number() == 5);
	REQUIRE(sheet.rules[0].declarations[1].property() == "property2");
	REQUIRE(sheet.rules[0].declarations[1].get().number() == 3);

	REQUIRE(sheet.rules[1].selector.items.size() == 1);
	REQUIRE(sheet.rules[1].selector.items[0].tag() == "tag2");
	REQUIRE(sheet.rules[1].selector.items[0].name() == "name2");
	REQUIRE(sheet.rules[1].declarations.size() == 2);
	REQUIRE(sheet.rules[1].declarations[0].property() == "property3");
	REQUIRE(sheet.rules[1].declarations[0].get().text() == "Hello");
	REQUIRE(sheet.rules[1].declarations[1].property() == "property4");
	REQUIRE(sheet.rules[1].declarations[1].get().number() == 25);
}

