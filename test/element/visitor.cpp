#include "../catch.hpp"
#include "layout/visitor.h"
#include "layout/layout.h"
#include "layout/element.h"
#include "layout/element/root.h"
#include "layout/element/label.h"
#include "layout/element/button.h"
#include "layout/element/select.h"
#include "layout/element/textbox.h"
#include "layout/element/container.h"

using namespace layout;
using namespace layout::element;

class TestVisitor : public Visitor {
public:
	virtual void visit(IVisitable* visitable) {
		log.push_back(((Element*)visitable)->tagName());
	}

	virtual void push(IVisitable* visitable) {
		log.push_back("push");
	}

	virtual void pop(IVisitable* visitable) {
		log.push_back("pop");
	}

	std::vector<std::string> log;
};

TEST_CASE("element visitor", "[element][visitor]") {
	TestVisitor* v = new TestVisitor();

	Layout l;
	Root* root = new Root(&l);
	Container* node1 = new Container();
	Button* node1_1 = new Button();
	Textbox* node1_2 = new Textbox();

	root->appendChild(node1);
	node1->appendChild(node1_1);
	node1->appendChild(node1_2);

	root->accept(v);

	std::string expected[] = {
		"push",
		"root",
		"push",
		"container",
		"push",
		"button",
		"pop",
		"push",
		"textbox",
		"pop",
		"pop",
		"pop"
	};

	REQUIRE(v->log.size() == sizeof(expected) / sizeof(expected[0]));
	for (unsigned int i = 0; i < v->log.size(); i++)
		REQUIRE(v->log[i] == expected[i]);

	delete root;
	delete v;
}

