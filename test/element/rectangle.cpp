#include "../catch.hpp"
#include "layout/layout.h"

using namespace layout;

TEST_CASE("element rectangle", "[element][rectangle]") {
	Layout layout;
	Element* container = Element::construct("container");

	layout.setViewportSize(Vec2(100, 100));

	layout.root()->appendChild(container);

	container->rectangle.left = 25;
	container->rectangle.right = 20;
	container->rectangle.y = 0;
	container->rectangle.height = 20;

	Box crect = container->rectangle.px();
	REQUIRE(crect.left() == Approx(-25.0f));
	REQUIRE(crect.right() == Approx(30.0f));
	REQUIRE(crect.width() == Approx(55.0f));
	REQUIRE(crect.center().x == Approx(2.5f));

	REQUIRE(crect.top() == Approx(-10.0f));
	REQUIRE(crect.bottom() == Approx(10.0f));
	REQUIRE(crect.height() == Approx(20.0f));
	REQUIRE(crect.center().y == Approx(0.0f));


}

