#include "catch.hpp"
#include "strutils.h"
#include "utf8.h"

using namespace layout::str;

TEST_CASE("read while", "[strutils][readWhile]") {
	std::string str ("str1,str2");
	iterator it = str.begin();
	REQUIRE(readWhile(it, str.end(), [](unsigned int c) -> bool { return c != ','; }) == "str1");
	utf8::next(it, str.end());
	REQUIRE(readWhile(it, str.end(), [](unsigned int c) -> bool { return c != ','; }) == "str2");
}

TEST_CASE("read while single", "[strutils][readWhile]") {
	std::string str ("str1");
	iterator it = str.begin();
	REQUIRE(readWhile(it, str.end(), [](unsigned int c) -> bool { return c != ','; }) == "str1");
}

TEST_CASE("read until", "[strutils][readUntil]") {
	std::string str ("str1,str2");
	iterator it = str.begin();
	REQUIRE(readUntil(it, str.end(), ',') == "str1");
	utf8::next(it, str.end());
	REQUIRE(readUntil(it, str.end(), ',') == "str2");
}

TEST_CASE("read until single", "[strutils][readUntil]") {
	std::string str ("str1");
	iterator it = str.begin();
	REQUIRE(readUntil(it, str.end(), ',') == "str1");
}

TEST_CASE("split", "[strutils][split]") {
	std::string str ("str1,str2,str3");
	std::vector<std::string> res = split(str, ',');
	REQUIRE(res.size() == 3);
	REQUIRE(res[0] == "str1");
	REQUIRE(res[1] == "str2");
	REQUIRE(res[2] == "str3");
}

TEST_CASE("split single", "[strutils][split]") {
	std::string str ("str1");
	std::vector<std::string> res = split(str, ',');
	REQUIRE(res.size() == 1);
	REQUIRE(res[0] == "str1");
}

TEST_CASE("split none", "[strutils][split]") {
	std::string str ("");
	std::vector<std::string> res = split(str, ',');
	REQUIRE(res.size() == 0);
}

TEST_CASE("trim whitespace", "[strutils]") {
	std::string str = "     	Test string 		    ";
	std::string res = trimWhitespace(str);
	REQUIRE(res == "Test string");
}

