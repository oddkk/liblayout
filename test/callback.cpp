#include "layout/callback.h"
#include "catch.hpp"

using namespace layout;

class CallbackTest
		: public ICallbackProvider<CallbackTest*, std::string> {
public:
	CallbackTest();
	Callback testCallback;

	std::string text;
};

void CallbackTest_testCallback(CallbackTest* self, std::string test) {
	self->text = test;
}

CallbackTest::CallbackTest()
	: testCallback("test", CallbackTest_testCallback) {
	bindCallback(testCallback);
}


TEST_CASE("callback", "") {
	CallbackTest test;

	test.raiseCallback("test", &test, "asdf");
	test.raiseCallback("asdf", &test, "asd1");
	REQUIRE(test.text == "asdf");
}

