#include "catch.hpp"
#include "layout/layout.h"
#include <vector>

using namespace layout;

static std::vector<std::string> called;

static void callback(Event& e) {
	called.push_back(((Element*)e.sender)->name);
}

static void callback_noprop(Event& e) {
	e.stopPropagation = true;
	called.push_back(((Element*)e.sender)->name);
}

static void compareVectors(std::vector<std::string> v1,
                           std::vector<std::string> v2) {
	REQUIRE(v1.size() == v2.size());
	for (unsigned int i = 0; i < v1.size(); i++) {
		REQUIRE(v1[i] == v2[i]);
	}
}

TEST_CASE("event", "[event]") {
	Layout l;

	l.load(
"<layout>"
	"<container name='cnt1'>"
		"<container name='cnt11'>"
			"<button name='btn1'/>"
		"</container>"
		"<container name='cnt12'>"
			"<button name='btn2'/>"
		"</container>"
	"</container>"
	"<container name='cnt2'>"
		"<container name='cnt21'>"
			"<button name='btn3'/>"
		"</container>"
	"</container>"
"</layout>");
	Element* cnt1 = l.select("#cnt1")[0];
	Element* cnt11 = l.select("#cnt11")[0];
	Element* btn1 = l.select("#btn1")[0];

	Element* cnt12 = l.select("#cnt12")[0];
	Element* btn2 = l.select("#btn2")[0];

	Element* cnt2 = l.select("#cnt2")[0];
	Element* cnt21 = l.select("#cnt21")[0];
	Element* btn3 = l.select("#btn3")[0];

	ElementList list = l.root()->decendents();

	list.on("event", callback);
	list.on("event_noprop", callback_noprop);

	SECTION("propegate") {
		std::vector<std::string> expected ({
			"btn1",
			"cnt11",
			"cnt1"
		});
		called.clear();
		btn1->raise("event");
		compareVectors(called, expected);
	}
	SECTION("no propegate") {
		std::vector<std::string> expected ({
			"btn1",
		});
		called.clear();
		btn1->raise("event_noprop");
		compareVectors(called, expected);
	}
}

