#include "catch.hpp"
#include "layout/classlist.h"

using namespace layout;

TEST_CASE("class list", "[class list]") {
	ClassList list;
	REQUIRE(list.isActive("class1") == false);
	REQUIRE(list.isActive("class2") == false);
	REQUIRE(list.to_string(".") == "");

	list.activate("class1");
	REQUIRE(list.isActive("class1") == true);
	REQUIRE(list.isActive("class2") == false);
	REQUIRE(list.to_string(".") == ".class1");

	list.activate("class2");
	REQUIRE(list.isActive("class1") == true);
	REQUIRE(list.isActive("class2") == true);
	REQUIRE(list.to_string(".") == ".class1.class2");

	list.deactivate("class1");
	REQUIRE(list.isActive("class1") == false);
	REQUIRE(list.isActive("class2") == true);
	REQUIRE(list.to_string(".") == ".class2");
}

TEST_CASE("class has", "[class list]") {
	ClassList list1;
	ClassList list2;
	ClassList list3;
	ClassList list4;

	list1.activate("class1");
	list1.activate("class2");

	list2.activate("class2");
	list2.activate("class3");

	list3.activate("class3");
	list3.activate("class4");

	list4.activate("class1");
	list4.activate("class2");
	list4.activate("class3");
	list4.activate("class4");

	SECTION("some active") {
		REQUIRE(list4.hasActive(list1));
		REQUIRE(list4.hasActive(list2));
		REQUIRE(list4.hasActive(list3));

		REQUIRE(list2.hasActive(list1));
		REQUIRE(list2.hasActive(list3));
		
		REQUIRE(!list1.hasActive(list3));
	}
	SECTION("all active") {
		REQUIRE(list4.hasAllActive(list1));
		REQUIRE(!list1.hasAllActive(list4));
	}
}
