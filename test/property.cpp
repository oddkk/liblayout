#include "catch.hpp"
#include "layout/layout.h"
#include "layout/element.h"
#include "layout/property.h"

using namespace layout;

class Dummy : public Element {
public:
	Dummy() {
		prop.bind(this, "prop");
	}
	virtual std::string tagName() const { return "dummy"; }

	Property prop;
};

TEST_CASE("property", "") {
	Layout layout;
	Dummy* e = new Dummy();

	layout.root()->appendChild(e);

	e->applyDeclaration(
		Declaration("prop",
		            Selector("dummy"),
		            expression::Expression("2")));


	REQUIRE((float)e->prop == 2.0f);

	e->applyDeclaration(
		Declaration("prop",
		            Selector(""),
		            expression::Expression("3")));

	REQUIRE((float)e->prop == 2.0f);

	e->applyDeclaration(
		Declaration("prop",
		            Selector("dummy.class"),
		            expression::Expression("5")));

	REQUIRE((float)e->prop == 2.0f);

	e->styleClass.activate("class");

	REQUIRE((float)e->prop == 5.0f);

	e->styleClass.deactivate("class");

	e->prop = 20.0f;

	REQUIRE((float)e->prop == 20.0f);

	e->prop.resetSet();

	REQUIRE((float)e->prop == 2.0f);

}
