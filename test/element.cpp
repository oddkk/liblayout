#include "catch.hpp"
#include "layout/layout.h"
#include "layout/element.h"

#include "layout/element/root.h"
#include "layout/element/button.h"
#include "layout/element/container.h"

#include <iostream>

using namespace layout;
using namespace layout::element;

TEST_CASE("element", "[element]") {
	Layout l;
	Root root (&l);
	Container* c1 = new Container();
	Container* c2 = new Container();
	Container* c3 = new Container();
	Button* b1 = new Button();
	Button* b2 = new Button();

	root.appendChild({ c1, c2, c3 });
	c1->appendChild(b1);
	c2->appendChild(b2);

	SECTION("children") {
		ElementList children = root.children();
		REQUIRE(children.has({ c1, c2, c3 }));
		REQUIRE(children.size() == 3);
	}

	SECTION("decendents") {
		ElementList decendents = root.decendents();
		REQUIRE(decendents.has({ c1, c2, c3, b1, b2 }));
		REQUIRE(decendents.size() == 5);
	}

	SECTION("siblings") {
		ElementList siblings = c1->siblings();
		REQUIRE(siblings.has({ c2, c3 }));
		REQUIRE(siblings.size() == 2);
	}

	SECTION("offset") {
		Element* e1 = c1->offset(2);
		REQUIRE(e1 == c3);

		Element* e2 = c3->offset(-2);
		REQUIRE(e2 == c1);

		Element* e3 = c3->offset(-3);
		REQUIRE(e3 == c1);

		Element* e4 = c1->offset(3);
		REQUIRE(e4 == c3);
	}

	SECTION("delete") {
		REQUIRE(c1->children().size() == 1);
		c1->deleteChild(b1);
		REQUIRE(c1->children().size() == 0);
	}
}

